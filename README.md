# Idle Time Prediction in Storage Systems with Machine Learning

## Description
This repository provides two Machine Learning models for predicting idle times in storage system. It contains two python project, a training environment for hyperparameter tuning and a execution environment for processing the workload in a multithreaded fashion.
For the TfLite model, a separate Jupyter Notebook is prepared for hyperparameter optimization.

## About the Framework
The settings for loading and preprocessing properly are stored in a separate JSON-file.
The pipeline from the execution environment of the Host-PC consists of initialising the memory and loading the workload, 
processing the workload and predicting the future IRT and is based on [Sibyl](https://github.com/CMU-SAFARI/Sibyl/blob/main/README.md).

## Prerequisites
Python: 3.10.12
* tensorflow 2.10.1
* keras 2.10.0
* keras-tcn 3.5.0
* pandas 1.5.3
* optuna 3.1.1
* scikit-learn 1.2.2
* tqdm 4.65.0
* matplotlib 3.7.1

## Commandline Arguments for Execution Environment
* `so_path`: Path to the .so-file for accessing the storage system
* `nof_threads`: Path to the .so-file for accessing the storage system
* `--workload`, `-w`: Name of the workload: "ycsb" or "msr-cambridge"
* `--workload_item`, `-i`: Workload_item: The options are available in params.json under "ycsb" and "msr-cambridge".
* `--tflite`: Whether to use the optimized TfLite model
* `--sim`: Whether to simulate the access to the storage system with accessing to a .txt file

## Commandline Arguments for Training Environment
* `--description`: Custom description of the model
* `--workload`, `-w`: Name of the workload: "ycsb" or "msr-cambridge"
* `--workload_item`, `-i`: Workload_item: The options are available in params.json under "ycsb" and "msr-cambridge".
* `--n_trials`: Number of trials with different combinations of hyperparameters
* `--evaluate`: Only evaluate model
* `--load_model`: Train with loaded model


 

 

