import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.models import Sequential
from keras.layers import LSTM
from keras.layers import Dense
from keras.callbacks import Callback, ModelCheckpoint, EarlyStopping
from keras.metrics import RootMeanSquaredError
from tqdm import tqdm
from pathlib import Path
from tcn import TCN
import time

from data_loading import inverse_scale

# fix random seed for reproducibility
seed = 7
tf.random.set_seed(seed)
tf.get_logger().setLevel('ERROR')

class StopTrainingOnThreshold(Callback):
    def __init__(self, threshold, patience):
        super(StopTrainingOnThreshold, self).__init__()
        self.threshold = threshold
        self.patience = patience
        self.wait = 0

    def on_epoch_end(self, epoch, logs=None):
        if logs is None:
            logs = {}
        if logs.get('loss') is not None and logs.get('loss') > self.threshold:
            self.wait += 1
            if self.wait >= self.patience:
                print(f"\nStopping training as loss ({logs.get('loss')}) exceeded the threshold ({self.threshold}).")
                self.model.stop_training = True
        else:
            self.wait = 0

def custom_loss(alpha):
    def loss_fn(y_true, y_pred):
        beta = 1.0  # Weight for penalizing too low predictions
        error = y_pred - y_true
        loss = tf.where(error < 0, alpha * tf.abs(error), beta * tf.abs(error))
        return tf.reduce_mean(loss)
    return loss_fn

def get_loss(model_loss, huber_delta, beta):
    # decide loss function
    if model_loss == "huber":
        return tf.keras.losses.Huber(delta=huber_delta, reduction="auto", name="huber_loss")
    elif model_loss == "mlse":
        return tf.keras.losses.MeanSquaredLogarithmicError(reduction="auto", name="mean_squared_logarithmic_error")
    elif model_loss == "mae":
        return "mae"
    elif model_loss == "custom":
        return custom_loss(beta)
    elif model_loss == "mse":
        return "mse"
    else:
        raise Exception ("Loss function not found")

def get_lr_schedule(lr_exp, lr_const, trainX, batch_size):
    # decide optimizer including learning rate function
    if lr_exp[0] == 1:
        return keras.optimizers.schedules.ExponentialDecay(
            initial_learning_rate=lr_exp[1],
            decay_steps=lr_exp[2],
            decay_rate=lr_exp[3])
    elif lr_const["enable"] == 1:
        b = [ep*len(trainX)//batch_size*0.8 for ep in lr_const["boundaries"]]
        return keras.optimizers.schedules.PiecewiseConstantDecay(b, lr_const["values"])
    else:
        print("learning_rate: 0.0001")
        return 0.0001

def get_optimizer(optimizer, lr_schedule):
    if optimizer == "adamax":
        return keras.optimizers.Adamax(learning_rate=lr_schedule)
    elif optimizer == "adagrad":
        return keras.optimizers.Adagrad(learning_rate=lr_schedule)
    elif optimizer == "adadelta":
        return keras.optimizers.Adadelta(learning_rate=lr_schedule)
    elif optimizer == "sgd":
        return keras.optimizers.SGD(learning_rate=lr_schedule)
    elif optimizer == "rmsprop":
        return keras.optimizers.RMSprop(learning_rate=lr_schedule)
    elif optimizer == "adam":
        return keras.optimizers.Adam(learning_rate=lr_schedule)
    else:
        raise Exception ("Optimizer not found")

def define_model_lstm(trainX, trainY, batch_size, optimizer, n, activation, lr_const, lr_exp, model_loss,
                 beta, huber_delta, stateful):
    model_sfull = Sequential()
    if stateful:
        model_sfull.add(LSTM(n[0], activation=activation, batch_size=batch_size,
                            input_shape=(trainX.shape[1], trainX.shape[2]), return_sequences=True, stateful=True))
        model_sfull.add(LSTM(n[1], activation=activation, batch_size=batch_size, return_sequences=False, stateful=True))
    else:
        model_sfull.add(LSTM(n[0], activation=activation, input_shape=(trainX.shape[1], trainX.shape[2]), return_sequences=True))
        model_sfull.add(LSTM(n[1], activation=activation, return_sequences=False))
    model_sfull.add(Dense(n[2], 'relu'))
    model_sfull.add(Dense(trainY.shape[1], "linear"))

    loss = get_loss(model_loss, huber_delta, beta)
    lr_schedule = get_lr_schedule(lr_exp, lr_const, trainX, batch_size)
    opt = get_optimizer(optimizer, lr_schedule)

    model_sfull.compile(optimizer=opt, loss=loss, metrics=[RootMeanSquaredError()])  # metrics=[r2_score]
    model_sfull.summary()
    return model_sfull


def define_model_tcn(trainX, trainY, batch_size, optimizer, nb_filters, kernel_size, max_dilation, nb_stacks, use_skip_connections,
                    activation, dropout_rate, use_batch_norm, use_layer_norm, use_weight_norm, lr_const, lr_exp,
                    model_loss, beta, huber_delta, kernel_initializer):
    powers_of_two = [2 ** i for i in range(max_dilation)]
    model = Sequential()
    model.add(TCN(input_shape=(trainX.shape[1], trainX.shape[2]), nb_filters=nb_filters, kernel_size=kernel_size,
                  nb_stacks=nb_stacks, return_sequences=False, dilations=powers_of_two, use_skip_connections=use_skip_connections,
                  activation=activation, dropout_rate=dropout_rate, use_batch_norm=use_batch_norm, 
                  use_layer_norm=use_layer_norm, use_weight_norm=use_weight_norm, kernel_initializer=kernel_initializer, padding="causal", ))
    model.add(Dense(1))

    loss = get_loss(model_loss, huber_delta, beta)
    lr_schedule = get_lr_schedule(lr_exp, lr_const, trainX, batch_size)
    opt = get_optimizer(optimizer, lr_schedule)

    model.compile(optimizer=opt, loss=loss, metrics=[RootMeanSquaredError()])  # metrics=[r2_score]
    model.summary()
    return model

def define_model_tcn_classifier(trainX, trainY, batch_size, optimizer, nb_filters, kernel_size, max_dilation, nb_stacks, lr_const, lr_exp,
                 model_loss,huber_delta):
    powers_of_two = [2 ** i for i in range(max_dilation)]
    model = Sequential()
    model.add(TCN(input_shape=(trainX.shape[1], trainX.shape[2]), nb_filters=nb_filters, kernel_size=kernel_size,
                  nb_stacks=nb_stacks, return_sequences=False, dilations=powers_of_two))
    model.add(Dense(5, activation='softmax'))

    lr_schedule = get_lr_schedule(lr_exp, lr_const, trainX, batch_size)
    opt = get_optimizer(optimizer, lr_schedule)

    model.compile(optimizer=opt, loss='categorical_crossentropy', metrics=['accuracy'])  # metrics=[r2_score]
    model.summary()
    return model    

def train_offline(model, offline_epochs, trainX, trainY, batch_size, earlystop_p, filepath):
    epochs = offline_epochs
    valX = trainX[int(len(trainX)*0.8):]
    valY = trainY[int(len(trainX)*0.8):]
    trainY = trainY[:int(len(trainX)*0.8)]
    trainX = trainX[:int(len(trainX)*0.8)]
    if model.layers[-1].output_shape[-1] == 1: #if regression
        monitor_score = "loss"
        monitor_val_score = "val_loss"
        shuffle_enable = False
        checkpoint_mode = "min"
    else:
        monitor_score = "accuracy"
        monitor_val_score = "val_accuracy"
        shuffle_enable = True
        checkpoint_mode = "max"

    stop_callback = StopTrainingOnThreshold(threshold=40, patience=4) #stop if loss is too high at beginning
    checkpoint = ModelCheckpoint(Path(filepath, "best_model.h5"), monitor=monitor_val_score, verbose=1, save_best_only=True,
                                 mode=checkpoint_mode)
    earlystop = EarlyStopping(monitor=monitor_val_score, min_delta=earlystop_p["min_delta"], patience=earlystop_p["patience"], verbose=1, mode='auto')    
    if tf.__version__ == "2.12.0":
        print("Using rl_reduce")
        from keras.callbacks import ReduceLROnPlateau
        reduce_lr = ReduceLROnPlateau(monitor=monitor_score, factor=0.7, patience=3, min_lr=8e-7)
        history = model.fit(trainX, trainY, batch_size=batch_size, validation_split=0.2,
                            epochs=epochs, verbose=1, shuffle=shuffle_enable, callbacks=[checkpoint, reduce_lr, earlystop, stop_callback])
    else:
        history = model.fit(trainX, trainY, batch_size=batch_size, validation_data=(valX,valY), epochs=epochs, verbose=1,
                            shuffle=shuffle_enable, callbacks=[checkpoint, earlystop, stop_callback])

    if model.layers[-1].output_shape[-1] == 1: #if regression
        train_metric = history.history["loss"]
        val_metric = history.history['val_loss']
    else: 
        train_metric = history.history["accuracy"]
        val_metric = history.history['val_accuracy']

    return [train_metric, val_metric], model

def contains_cell(model, celltype):
    for layer in model.layers:
        if isinstance(layer, celltype):
            return True
    return False

def set_lr(lr, model):
    if "PiecewiseConstantDecay" in str(model.optimizer.lr):
        model.optimizer.lr.values=[lr]* len(model.optimizer.lr.values)
    else:
        model.optimizer.lr.assign(lr)
            
    return model

def train_online(model, testX, testY, scaler, online_train_interval, log_irt, online_lr):
    model = set_lr(online_lr, model)
    if contains_cell(model, LSTM):
        model.reset_states()
    # Online training (update model with each new data available):
    y_pred = []
    loss = []
    rmse = []
    train_timer = 0  # training interval, set initial value to 0 to train with the first batch
    Y = []
    X = []
    batch_size=1
    #train process
    for t in tqdm(range(testY.shape[0] // batch_size)):
        x = testX[t * batch_size:(t + 1) * batch_size]
        #start = time.perf_counter()
        prediction = model.predict_on_batch(x)  # predict on the "new" input
        #print(time.perf_counter()-start)
        prediction[prediction <=-1] = -1
        prediction[prediction >=1] = 1
        prediction = inverse_scale(scaler, prediction)
        X.append(x[0])
        if log_irt:  # if log transform is applied to the irt
            y_pred.extend(list(np.exp(prediction)))
        else:
            y_pred.extend(list(prediction))

        y = testY[t * batch_size:(t + 1) * batch_size].reshape(-1, 1)  # a "new" label is available
        Y.append(y[0])
        if train_timer == 0:
            score = model.fit(np.array(X), np.array(Y),batch_size=batch_size, epochs=1, verbose=0).history  # runs a single gradient update
            loss.append(score["loss"])
            rmse.append(score["root_mean_squared_error"])
            train_timer = online_train_interval
            Y = []
            X = []
        train_timer = train_timer - batch_size

    return [loss, rmse], model, y_pred