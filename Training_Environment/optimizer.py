import os
from pathlib import Path
import json
from tensorflow import keras

from data_loading import load_workload
from model import define_model_lstm, define_model_tcn, define_model_tcn_classifier, train_online, train_offline, custom_loss, contains_cell
from evaluation import evaluate_online, save_model_stats, plot_predictions_detailed, predict, load_environment
from plotting import *
import optuna
import time
import joblib
from tcn import TCN
from keras.metrics import RootMeanSquaredError


value_mapping = {
    0: 0,
    1: 0.000001,
    2: 0.0001,
    3: 0.01,
    4: 0.1,
    5: 1
}

# part of hyperparameter tuning framework, called "optuna"
class Objective:
    def __init__(self, args, folder_path):
        os.system('echo "Start Training"')
        # Hold this implementation specific arguments as the fields of the class.
        self.args = args
        self.folder_path = folder_path

    # SPECIFY THE HYPERPARAMETER HERE :
    def __call__(self, trial):
        folder_path = self.folder_path
        with open(Path("params.json")) as f:
            s = json.load(f)

        #Parameter
        earlystop = s["earlystop"]
        #batch_size = s["df_shape"]["batch_size"] #loaded from external json file
        df_part = s["df_part"]

        if s["hyperparam"]["tcn"]["classify"] == 1:
            self.args.log_irt = False

        #Hyperparameter
        h = s["hyperparam"]
        #Dataset hyperparameter
        n_past = trial.suggest_int("n_past", h["n_past"][0], h["n_past"][1])
        batch_size_exp = trial.suggest_int("batch_size_exp", h["batch_size_exp"][0], h["batch_size_exp"][1])
        batch_size = 2**batch_size_exp

        #Model Compilation Hyperparameter
        offline_epochs = trial.suggest_int('epochs', h["epochs"][0], h["epochs"][1])
        online_train_interval = trial.suggest_int('online_train_interval', h["online_train_interval"][0], h["online_train_interval"][1], 
                                                  step=1, log=True)
        online_lr = trial.suggest_float("online_lr", h["online_lr"][0], h["online_lr"][1])
        i_lr = trial.suggest_float("i_lr", h["lr_exp"]["i_lr"][0], h["lr_exp"]["i_lr"][1])
        dec_steps = trial.suggest_int("dec_steps", h["lr_exp"]["dec_steps"][0], h["lr_exp"]["dec_steps"][1])
        dec_rate = trial.suggest_float("dec_rate", h["lr_exp"]["dec_rate"][0], h["lr_exp"]["dec_rate"][1])
        optimizer = trial.suggest_categorical("optimizer", h["optimizer"])
        model_loss = trial.suggest_categorical("model_loss", h["model_loss"])
        beta = trial.suggest_int('beta', h["beta"][0], h["beta"][1],3)
        huber_delta = trial.suggest_float("huber_delta", h["huber_delta"][0], h["huber_delta"][1])

        #save input arguments
        save_args(folder_path, self.args)

        if h["lstm"]["enable"]==1 and batch_size != 1:
            print("Batch is forced to 1 since LSTM model is chosen")
            batch_size=1

        if self.args.load_model: #load model
            s, args, study_params, trial_nr, model_folder = load_environment(self.args)
            args.workload = self.args.workload
            args.workload_item = self.args.workload_item
            try:
                batch_size = 2**study_params["batch_size_exp"]
            except:
                batch_size = 2**s["hyperparam"]["batch_size_exp"]

            df_train, df_test, trainX, trainY, testX, testY, scaler = load_workload(args, study_params["n_past"], batch_size,
                                                                                    s["df_part"], folder_path)

            #open trained model
            custom_objects = {}
            if s["hyperparam"]["tcn"]["enable"] == 1:
                custom_objects["TCN"]=TCN
            if study_params["model_loss"] == "custom":
                custom_objects["loss_fn"] = custom_loss(beta)
            model_path = Path(model_folder, "model", "pretrained")
            model = keras.models.load_model(Path(model_path,"best_model.h5"), custom_objects=custom_objects, compile = False)
            model.compile(optimizer = keras.optimizers.Adam(learning_rate=online_lr), loss = custom_loss(beta), metrics=[RootMeanSquaredError()])

            #create folder for online training results within the folder of the trained model
            res_folder_path = Path(folder_path, str(trial.number))
            os.mkdir(res_folder_path)
            del model_path

            # predict on test data
            # test_true, test_pred_offline, y_pred_offline_scaled, er = predict(model, testX, testY, df_test, batch_size, start=0, end=len(testX), 
            #                                                         n_past=study_params["n_past"], log_irt=args.log_irt, scaler=scaler)
            # plot_predictions_detailed(test_true, test_pred_offline, res_folder_path, description="Offline_prediction_test", show_plot=False)

            # do online training and evaluate result
            train_true, train_pred, model_warm, er = evaluate_online(model, df_train, trainX, trainY, batch_size, 
                                                                                 scaler, study_params["n_past"], online_train_interval, args.log_irt,
                                                                                 online_lr)
            model_warm.save(Path(res_folder_path, "model_warm.h5"))
            plot_predictions_detailed(train_true, train_pred, res_folder_path, description = "Online_training_unscaled_train", show_plot=False)

            test_true, test_pred, model_evaluated, er = evaluate_online(model_warm, df_test, testX, testY, batch_size, 
                                                                                 scaler, study_params["n_past"], study_params["online_train_interval"], args.log_irt,
                                                                                 online_lr)
            model_evaluated.save(Path(res_folder_path, "model_evaluated.h5"))
            plot_predictions_detailed(test_true, test_pred, res_folder_path, description = "Online_training_unscaled_test", show_plot=False)
            
            # save sample of dataset and params
            dest_path = Path(folder_path, self.args.workload_item+"_sample.csv")
            df_train[:10].to_csv(dest_path, index=False)
            with open(Path(folder_path, "params.json"), 'w') as f:
                json.dump(s, f)

            p = trial.params
            p["score"] = er
            with open(Path(res_folder_path, "study_params.json"), 'w') as f:
                json.dump(p, f)
            return er

        else:
            #load Dataset
            df_train, df_test, trainX, trainY, testX, testY, scaler = load_workload(self.args, n_past, batch_size, df_part, folder_path)

            #Define Model according to hyperparameters
            if s["hyperparam"]["lstm"]["enable"] == 1: #if lstm model
                h_lstm = s["hyperparam"]["lstm"]
                n1 = trial.suggest_int("n1", h_lstm["n1"][0], h_lstm["n1"][1], h_lstm["n1"][2])
                n2 = trial.suggest_int("n2", h_lstm["n2"][0],h_lstm["n2"][1], h_lstm["n2"][2])
                n3 = trial.suggest_int("n3", h_lstm["n3"][0], h_lstm["n3"][1], h_lstm["n3"][2])
                activation = trial.suggest_categorical("activation", h_lstm["activation"])
                # activation = trial.suggest_categorical("activation", ["tanh"])
                model = define_model_lstm(trainX, trainY, batch_size,
                                    optimizer=optimizer, n=[n1, n2, n3], activation=activation,
                                    lr_exp=[h["lr_exp"]["enable"], i_lr, dec_steps, dec_rate], lr_const=h["lr_const"],
                                    model_loss=model_loss, beta=beta, huber_delta=huber_delta, stateful=s["hyperparam"]["lstm"]["stateful"])
            else:
                h_tcn = s["hyperparam"]["tcn"] #if tcn model
                nb_filters = trial.suggest_int("nb_filters", h_tcn["nb_filters"][0], h_tcn["nb_filters"][1], h_tcn["nb_filters"][2])
                kernel_size = trial.suggest_int("kernel_size", h_tcn["kernel_size"][0], h_tcn["kernel_size"][1], h_tcn["kernel_size"][2])
                max_dilation = trial.suggest_int("max_dilation", h_tcn["max_dilation"][0], h_tcn["max_dilation"][1], h_tcn["max_dilation"][2])
                nb_stacks = trial.suggest_int("nb_stacks", h_tcn["nb_stacks"][0], h_tcn["nb_stacks"][1], h_tcn["nb_stacks"][2])
                use_skip_connections = trial.suggest_categorical("use_skip_connections", h_tcn["use_skip_connections"])
                activation = trial.suggest_categorical("activation", h_tcn["activation"])
                dropout_rate = trial.suggest_float("dropout_rate", h_tcn["dropout_rate"][0], h_tcn["dropout_rate"][1])
                norm = trial.suggest_int("norm", h_tcn["norm"][0], h_tcn["norm"][1])
                kernel_initializer = trial.suggest_categorical("kernel_initializer", h_tcn["kernel_initializer"])
                norm_vec = np.zeros(3)
                if norm < 3:                 #[0,0,0] = no normalization
                    norm_vec[norm] = 1
                use_batch_norm = norm_vec[0] #[1,0,0]
                use_layer_norm = norm_vec[1] #[0,1,0]
                use_weight_norm = norm_vec[2]#[0,0,1]

                if s["hyperparam"]["tcn"]["classify"] == 1: #if classification 
                    model = define_model_tcn_classifier(trainX, trainY, batch_size,
                                            optimizer,nb_filters, kernel_size, max_dilation, nb_stacks, lr_const=h["lr_const"],
                                            lr_exp=[h["lr_exp"]["enable"],i_lr, dec_steps, dec_rate],
                                            model_loss=model_loss, huber_delta=huber_delta)
                else:
                    model = define_model_tcn(trainX, trainY, batch_size, #if regression
                            optimizer,nb_filters, kernel_size, max_dilation, nb_stacks, use_skip_connections,
                            activation, dropout_rate, use_batch_norm, use_layer_norm, use_weight_norm, 
                            lr_const=h["lr_const"], lr_exp=[h["lr_exp"]["enable"],i_lr, dec_steps, dec_rate],
                            model_loss=model_loss, beta=beta, huber_delta=huber_delta, kernel_initializer=kernel_initializer)
            
            # if model.count_params() > 2e6: #only take models with less than 1mio params
            #     raise optuna.TrialPruned()

        model_path = Path(folder_path, str(trial.number))

        #Save sample of dataset and params
        dest_path = Path(model_path, "dataset_sample.csv")
        os.mkdir(model_path)
        df_train[:10].to_csv(dest_path, index=False)
        with open(Path(folder_path, "params.json"), 'w') as f:
            json.dump(s, f)

        #Offline and Online Training
        results, final_loss = training(model, offline_epochs, df_train, df_test,
                              trainX, trainY, testX, testY,
                              scaler, batch_size, n_past,
                              earlystop, online_train_interval, online_lr, beta, model_path, self.args.log_irt, self.args.take_best_model)
        joblib.dump(trial.study, Path(folder_path, "study.pkl"))
        p = trial.params
        p["score"] = results
        with open(Path(model_path, "study_params.json"), 'w') as f:
            json.dump(p, f)
        return final_loss

def class_to_value(class_data):
    y_pred = []
    for i in class_data:
        predicted_class = np.argmax(i)
        y_pred.append(value_mapping[predicted_class])
    
    return y_pred


def training(model_raw, offline_epochs, df_train, df_test,
                              trainX, trainY, testX, testY,
                              scaler, batch_size, n_past,
                              earlystop, online_train_interval, online_lr, beta, model_path, log_irt, take_best_model):

    history, model = train_offline(model_raw, offline_epochs, trainX, trainY, batch_size, earlystop, Path(model_path, "pretrained"))
    model.save(Path(model_path, "pretrained", "last_model.h5"))

    if take_best_model:#either take best model during training or the last model after finishing training
        custom_objects = {}
        if contains_cell(model, TCN):
            custom_objects["TCN"]=TCN
        if "loss_fn" in str(model.loss):
            custom_objects["loss_fn"] = custom_loss(beta)
        print("taking best model")
        model = keras.models.load_model(Path(model_path,"pretrained","best_model.h5"), custom_objects=custom_objects)

    #Offline prediction
    if model.layers[-1].output_shape[-1] != 1: #classification
        test_pred_class = model.predict(testX)
        test_pred = class_to_value(test_pred_class)
        loss_online, accuracy = model.evaluate(testX, testY, verbose=1)
        test_true = df_test.iloc[n_past-1:-1]['future inter request time (s)'].to_numpy()
        test_true = [value_mapping[value] for value in test_true]
        print("Test loss:", loss_online)
        print("Test accuracy:", accuracy)
    else:
        #Evaluate offline with testset
        test_true, test_pred, test_pred_scaled, test_loss = predict(model, testX, testY, df_test, batch_size, start=0, end=len(testX), n_past=n_past, log_irt=log_irt, scaler=scaler)
        plot_predictions(testY, test_pred_scaled, model_path, description="Offline_prediction_scaled_test", show_plot=False)
        plot_predictions_detailed(test_true, test_pred, model_path, description="Offline_prediction_test", show_plot=False)
        
        #Evaluate online with testset
        test_true, test_pred, model_final, test_loss_online = evaluate_online(model, df_test, testX, testY, batch_size, scaler, n_past, online_train_interval, log_irt, online_lr)
        model_final.save(Path(model_path, "final"))
        plot_predictions_detailed(test_true, test_pred, model_path, description = "Online_evaluation_unscaled_test", show_plot=True)

        #evaluate online with trainset
        #train_true, train_pred, _ = evaluate_online(model, df_train, trainX, trainY, batch_size, scaler, n_past, online_train_interval, log_irt, online_lr)
        #plot_predictions_detailed(train_true, train_pred, model_path, description = "Online_evaluation_unscaled_train", show_plot=False)

    results = {}
    try:
        results["offline_rmse"] = test_loss[0]
        results["offline_loss"] = test_loss[1]
        final_loss = test_loss[1]
    except:
        pass

    try:
        results["online_rmse"] = test_loss_online[0]
        results["online_loss"] = test_loss_online[1]
        final_loss = test_loss_online[1]
    except:
        pass

    save_model_stats(history, final_loss, online_train_interval, test_pred, model_path)
    return results, final_loss

def save_args(folder_path, args):
    with open(Path(folder_path,'args.json'), 'w') as f:
        json.dump(vars(args), f)