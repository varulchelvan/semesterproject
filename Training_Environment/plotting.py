import matplotlib
from pathlib import Path
import numpy as np
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import os

def plot_predictions(y_true, y_pred, model_path, description, show_plot):
    assert len(y_true) == len(y_pred)
    plt.plot(y_true, label="true")
    plt.plot(y_pred, label="pred")
    plt.legend()
    plt.title(description.replace("_", " "))
    plt.savefig(Path(model_path, description+"prediction.png"))
    if show_plot:
        plt.show()
    plt.close()
    return

def plot_predictions_detailed(y_true, y_pred, model_path, description, show_plot):
    os.mkdir(Path(model_path, description))

    assert len(y_true)==len(y_pred)
    print("Plot " + description.replace("_", " "))
    plt.yscale('log')
    plt.plot(abs(y_true), label="True IRT")
    plt.plot(abs(y_pred), label="Predicted IRT")
    plt.xlabel("Request index (no unit)")
    plt.ylabel("Absolute Inter Request Time [s]")
    plt.title(description.replace("_", " "))
    plt.legend()
    plt.savefig(Path(model_path, description ,description+".png"))
    if show_plot:
        plt.show()
    plt.close()

    np.save(Path(model_path, description,'y_true.npy'), y_true)
    np.save(Path(model_path, description,'y_pred.npy'), y_pred)

    # plot different parts of the trace and the corresponding MSE
    l = len(y_true)
    plt_range = min(70, l // 3 - 1)

    # define appropriate locations for plotting: highest irt, lowest irt, random
    idx_high_irt = [np.argmax(y_true[plt_range // 2:l // 3]) + plt_range // 2,
                    # since only idx after plt_range/2 are considered, resulting idx has to be corrected
                    np.argmax(y_true[l // 3:2 * l // 3]) + l // 3,
                    np.argmax(y_true[2 * l // 3:l - plt_range // 2]) + 2 * l // 3]
    idx_low_irt = [np.argmin(y_true[plt_range // 2:l // 3]) + plt_range // 2,
                   np.argmin(y_true[l // 3:2 * l // 3]) + l // 3,
                   np.argmin(y_true[2 * l // 3:l - plt_range // 2]) + 2 * l // 3]
    idx_random_irt = list(np.random.randint(plt_range // 2, l - plt_range // 2 - 1, size=3))
    idx = [idx_high_irt, idx_low_irt, idx_random_irt]

    fig = plt.figure(constrained_layout=True)
    fig.suptitle(description.replace("_", " ")+": Predicted IRT=Orange, True IRT=Blue")
    fig.set_size_inches(24, 14)
    row_title = ["High inter request time", "Low inter request", "Random"]
    # create 3x1 subfigs
    subfigs = fig.subfigures(nrows=3, ncols=1)
    for row, (subfig, idx_row) in enumerate(zip(subfigs, idx)):
        subfig.suptitle(f'{row_title[row]}')
        # create 1x3 subplots per subfig
        axs = subfig.subplots(nrows=1, ncols=3)
        for col, (ax, idx) in enumerate(zip(axs, idx_row)):
            begin = idx - plt_range // 2
            end = idx + plt_range // 2
            x_ = range(begin, end)
            y_t = y_true[begin:end]
            y_p = y_pred[begin:end]
            ax.plot(x_, y_t, label="true", color='blue')
            ax.plot(x_, y_p, label="pred", color="orange")
            ax.set_xlabel("Request index (no unit)")
            ax.set_ylabel("Inter Request Time [s]")
            ax.set_title(f'MSE {np.square(np.subtract(y_t, y_p)).sum()}')
            np.save(Path(model_path, description, str(row*3+col)+'_idx.npy'), np.array([begin,end]))
    fig.savefig(Path(model_path, description,description+"eval.png"))
    if show_plot:
        plt.show()
    plt.close()

def plot_training_results(train_loss_online, train_loss_offline, val_loss_offline, online_train_interval, model_path):
    train_loss = np.append(train_loss_offline, train_loss_online)

    # determine necessary lengths
    offline_train_len = len(train_loss_offline)
    online_train_len = len(train_loss_online) * online_train_interval

    # create x-axis
    eps_offline = np.arange(1, offline_train_len + 1)
    eps = np.append(eps_offline,
                    np.arange(offline_train_len + 1, offline_train_len + online_train_len + 1, online_train_interval))
    eps_label = np.append(eps_offline, np.arange(1, online_train_len + 1, online_train_interval))

    step_size = max(1,len(train_loss_offline)//10)
    # plot
    img_path = Path(model_path, "loss.jpg")
    plt.yscale('log')
    plt.plot(eps, train_loss)
    plt.xticks(eps[::step_size], eps_label[::step_size])
    plt.plot(eps_offline, val_loss_offline)
    plt.axvline(x=offline_train_len, color="g", label='axvline - full height')
    plt.title('Model Performance')
    plt.ylabel('Loss')
    plt.xlabel('Epoch')
    plt.legend(['Training Loss', 'Validation loss'], loc='upper right')
    plt.savefig(img_path)
    #plt.show()
    plt.close()