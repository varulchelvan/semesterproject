import argparse
from data_loading import *
from optimizer import Objective
import optuna
import joblib
import json
import os

from evaluation import evaluate

def compose_folder_path(args):
    with open(Path("params.json")) as f:
        s = json.load(f)
    
    if args.load_model:
        prefix = s["folder_path_model"]
        desc = "online" + str(int(time.time_ns() / 1e9))
        folder_path = Path(prefix,desc)
        os.mkdir(folder_path)
        return folder_path
    else:
        desc = "log_" if args.log_irt else "not_log_"  # description name for saving folder
        desc = args.workload + "_" + desc
        desc = args.workload_item + "_" + desc
        desc = desc+ "lstm" if s["hyperparam"]["lstm"]["enable"]==1 else desc+"tcn"
        desc = desc + args.description + "_"# custom change of description
        folder_path = Path(s["paths"]["dest_prefix_loc"], desc + str(int(time.time_ns() / 1e9)))
        os.mkdir(folder_path)
        print(desc)
        return folder_path


def main(args):
    if args.evaluate:
        evaluate(args)
    else:
        folder_path = compose_folder_path(args)
        study = optuna.create_study(direction='minimize')
        try:
            study.optimize(Objective(args, folder_path), n_trials=int(args.n_trials))
        except KeyboardInterrupt:
            joblib.dump(study, Path(folder_path, "study.pkl"))

        joblib.dump(study, Path(folder_path, "study.pkl"))


# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--description", required=True)
    parser.add_argument("-i", "--workload_item", required=True)
    parser.add_argument("-w", "--workload", required=True)
    parser.add_argument("-t", "--n_trials", required=True)
    parser.add_argument("--take_best_model", action="store_true")
    parser.add_argument("--log_irt", action="store_true")
    parser.add_argument("-eval", "--evaluate", action="store_true")
    parser.add_argument("-load_m", "--load_model", action="store_true")
    args = parser.parse_args()
    main(args)
