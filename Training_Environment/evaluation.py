import joblib
import json
import os
import time
from argparse import Namespace
import numpy as np
from sklearn.metrics import mean_squared_error as mse

from plotting import *
from model import *
from data_loading import load_workload
from tcn import TCN


def evaluate_online(model, test, X, Y, batch_size, scaler, n_past, online_train_interval, log_irt, online_lr, start= 0, end = None):
    if end == None:
        end = len(X)
    X = X[start:end]
    Y = Y[start:end]
    loss, model_final, y_pred = train_online(model, X, Y, scaler, online_train_interval, log_irt, online_lr)
    if log_irt:
        y_true = np.exp(test.iloc[n_past-1:-1]['future inter request time (s)'].to_numpy())
    else:
        y_true = test.iloc[n_past-1:-1]['future inter request time (s)'].to_numpy()

    y_true  = y_true[start:end]
    assert len(y_pred) == len(Y)
    assert len(y_true) == len(y_pred)
    rmse = np.sqrt(mse(y_true, y_pred))
    return y_true, np.array(y_pred), model_final, [rmse, loss]

def save_model_stats(history, loss_online, online_train_interval, y_pred_future_test, model_path):
    #convert to np array
    train_loss_online = np.array(loss_online).flatten()
    train_loss_offline = np.array(history[0]).flatten()
    val_loss_offline = np.array(history[1]).flatten()
    plot_training_results(train_loss_online, train_loss_offline, val_loss_offline, online_train_interval, model_path)
    scores = {
      "train_loss_offline": train_loss_offline,
      "val_loss_offline": val_loss_offline,
      "train_loss_online": train_loss_online,
      "testY_pred": y_pred_future_test
    }
    scores_path = Path(model_path,"scores.pkl")
    joblib.dump(scores, scores_path)

def load_environment(new_args):
    #open params.json of the project to get the folder path for the params.json of the trained model
    with open(Path("params.json")) as f:
        s = json.load(f)
    folder_path = s["folder_path_model"]
    df_part = s["df_part"]
    
    with open(Path(folder_path, "params.json")) as f:
        s = json.load(f)
    s["folder_path_model"]=folder_path #transfer model path
    s["df_part"] = df_part
    #get input arguments of the trained model
    with open(Path(folder_path, "args.json")) as f:
        args = json.load(f)
    args = Namespace(**args)
    args.workload = new_args.workload
    args.workload_item = new_args.workload_item
    
    #if the study.pkl exists, it will be used, otherwise some params are assumed
    try:
        t = Path(folder_path,"model","study_params.json")
        with open(t) as f:
            study_params = json.load(f)
        trial_nr=0
    except:
        print("Could load study, taking trial 1, n_past=100")
        trial_nr = 0
        study_params = {}
        study_params["n_past"]=s["hyperparam"]["n_past"][0]
        study_params["online_train_interval"] = s["hyperparam"]["online_train_interval"][0]
        study_params["online_lr"] = s["hyperparam"]["online_lr"][0]
        study_params["model_loss"] = s["hyperparam"]["model_loss"][0]
        study_params["beta"] = s["hyperparam"]["beta"][0]

    return s, args, study_params, trial_nr, folder_path

def evaluate(new_args):
    s, args, study_params, trial_nr, folder_path = load_environment(new_args)
    try:
        batch_size = 2**study_params["batch_size_exp"]
    except:
        batch_size = 2**s["hyperparam"]["batch_size_exp"][0]
    df_train, df_test, trainX, trainY, testX, testY, scaler = load_workload(args, study_params["n_past"], batch_size,
                                                                            s["df_part"], folder_path)
    #open trained model
    custom_objects = {}
    if s["hyperparam"]["tcn"]["enable"] == 1:
        custom_objects["TCN"]=TCN
    if study_params["model_loss"] == "custom":
        custom_objects["loss_fn"] = custom_loss(study_params["beta"])

    if "online" in folder_path:
        model_path = Path(folder_path, str(trial_nr))
        model = keras.models.load_model(model_path, custom_objects=custom_objects)
    else:
        try: 
            model_path = Path(folder_path, str(trial_nr), "final")
            model = keras.models.load_model(model_path, custom_objects=custom_objects)
        except:
            model_path = Path(folder_path, str(trial_nr), "pretrained")
            model = keras.models.load_model(Path(model_path,"best_model.h5"), custom_objects=custom_objects, compile = False)
            model.compile(optimizer = keras.optimizers.Adam(learning_rate=0.001), loss = custom_loss(12), metrics=[RootMeanSquaredError()])

    #create folder for evaluation results within the folder of the trained model
    eval_folder_path = Path(model_path,"eval"+str(int(time.time_ns() / 1e9)))
    os.mkdir(eval_folder_path)
    del model_path
    model.save(Path(eval_folder_path, "best_model.h5"))

    # predict on test data
    test_true, test_pred_offline, y_pred_offline_scaled, er = predict(model, testX, testY, df_test, batch_size, start=0, end=1000, 
                                                              n_past=study_params["n_past"], log_irt=args.log_irt, scaler=scaler)
    plot_predictions_detailed(test_true, test_pred_offline, eval_folder_path, description="Offline_prediction_test", show_plot=True)

    # do online training and evaluate result
    test_true, test_pred, model_evaluated, loss_online = evaluate_online(model, df_test, testX, testY, batch_size, scaler, study_params["n_past"],
                  study_params["online_train_interval"], args.log_irt, study_params["online_lr"])
    model_evaluated.save(Path(eval_folder_path))
    plot_predictions_detailed(test_true, test_pred, eval_folder_path, description = "Online_evaluation_unscaled_test", show_plot=False)

    #train_true, train_pred, _ = evaluate_online(model, df_train, trainX, trainY, s["df_shape"]["batch_size"], scaler, params["n_past"],
    #               params["online_train_interval"], args.log_irt)
    #plot_predictions_detailed(train_true, train_pred, eval_folder_path, description = "Online_evaluation_unscaled_train", show_plot=True)
    return er

def predict(model, X, Y, test, batch_size, start, end, n_past, log_irt, scaler):
    X = X[start:end]
    Y = Y[start:end]
    start_time = time.perf_counter()
    y_pred_scaled = model.predict(X, batch_size=batch_size, verbose=1).flatten()
    print(f"mean inference time: {(time.perf_counter()-start_time)/len(X)}")
    y_pred_scaled = y_pred_scaled.reshape(-1, 1)

    loss = model.evaluate(X, Y, batch_size=batch_size, verbose=0)

    if log_irt:
        y_pred = np.exp(inverse_scale(scaler, y_pred_scaled))
        if model.layers[-1].output_shape[-1] == 1:
            y_pred = [min(y, 4) for y in y_pred] # set max irt 4
            y_pred = np.array(y_pred)
        y_check = np.exp(inverse_scale(scaler, Y))
        y_true = np.exp(test.iloc[n_past-1:-1]['future inter request time (s)'].to_numpy())
    else:
        y_pred = inverse_scale(scaler, y_pred_scaled)
        y_check = inverse_scale(scaler, Y)
        y_true = test.iloc[n_past-1:-1]['future inter request time (s)'].to_numpy()
    y_true = y_true[start:end]


    assert np.isclose(y_true,y_check, atol=1e-5).all() # check if y_true is really the target array
    assert len(y_pred) == len(X)
    rmse = np.sqrt(mse(y_true, y_pred))
    return y_true, y_pred, y_pred_scaled, [rmse, loss[0]]