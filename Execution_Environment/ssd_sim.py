import time
class ssd_sim():
    def openFastDevice(self):
        print("Simulation")
        ff = open('artifacts/Device.txt', "wb")
        return ff

    def openSlowDevice(self):
        fs = open('artifacts/Device.txt', "wb")
        return fs

    def sibyl_write(self, fd, byte_offset, nSize):
        arr = bytes(bytearray([0xa5] * nSize))
        with open("artifacts/Device.txt", "wb") as f:
            f.write(arr)
        return nSize

    def sibyl_read(self, fd, byte_offset, nSize):
        with open("artifacts/Device.txt", "rb") as f:
            byte =f.read(nSize)
        return nSize

    def qrator_write(self, fd, byte_offset, nSize):
        arr = bytes(bytearray([0xa5] * nSize))
        with open("artifacts/Device.txt", "wb") as f:
            f.write(arr)
        return nSize

    def qrator_read(self, fd, byte_offset, nSize):
        with open("artifacts/Device.txt", "rb") as f:
            byte =f.read(nSize)
        return nSize

    def closeDevice(self, fd):
        time.sleep(100/1000000000)