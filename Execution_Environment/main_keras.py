import pandas as pd
import numpy as np
import time
import request
import queue
from model import *
from pathlib import Path
from hybridstorage import HybridStorage
from hybridstorageenvironment import HybridStorageEnvironment
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter
from ssd_sim import *
from ctypes import *
import os
import threading

import sys

queueLock = threading.Lock()
buffer = queue.Queue()
workQueue = None
latencies = np.zeros(1) #dummy initialization
ssd_delays = np.zeros(1) #dummy initialization
inter_request_time = np.zeros(1)
queue_empty = False


class requestThread(threading.Thread):
    def __init__(self, threadID, q, environment, model, data_scaled, track_ssd_feature,
                 sim, so_path, use_tflite, scaler, log_irt, modelLock):
        X,Y = data_scaled
        threading.Thread.__init__(self)
        self.ID = threadID
        self.q = q
        self.modelLock = modelLock
        self.data = None
        self.index = None
        self.environment = environment
        self.model = model
        self.X = X
        self.Y = Y
        self.scaler = scaler
        self.log_irt = log_irt
        self.use_tflite = use_tflite
        self.my_functions = ssd_sim() if sim else CDLL(so_path)
        self.fastDevice = self.my_functions.openFastDevice()
        #self.slowDevice = self.my_functions.openSlowDevice()
        self.track_ssd_feature = track_ssd_feature
        self.dataset = self.environment._hybrid._df if self.track_ssd_feature else self.environment._df

    def run(self):
            time.sleep(2)
            #print("Starting " + self.name)
            process_data(self)
            #print("Exiting " + self.name)

    def placement_s(self, data):  # shrinked version of placement --> for idle time calculation only
        if (data[2] == 1):
            #start = time.perf_counter()
            lat = self._reqLatency = self.my_functions.sibyl_write(self.fastDevice, str(data[0]), int(data[1]))
            #time.sleep(1e-3)
            #end = time.perf_counter()
        else:
            #start = time.perf_counter()
            lat = self.my_functions.sibyl_read(self.fastDevice, str(data[0]), int(data[1]))
            #time.sleep(1e-5)
            #end = time.perf_counter()
        self._final_latency = lat
        #s = "write" if data[2] == 1 else "read"
        #print(f"C Latency: {lat} \n {s}")
        return self._final_latency

def predict_inter_request_time(self):
    irt = predict_online(self.modelLock, self.model, self.X[self.data["index"]], self.scaler, self.log_irt) #model, X, scaler, log_irt
    return irt

def process_data(self):
    global latencies
    global data
    data = self.dataset.iloc[0]
    data["future inter request time (s)"] = 0
    last_req = len(self.environment._hybrid._df) if self.track_ssd_feature else len(self.environment._df)

    while True:
        queueLock.acquire()
        if data["index"] == last_req-1: #check if last request is arrived
            queueLock.release()
            return
        if data["future inter request time (s)"] > 1e-3:  # sleep with control loop
            #latencies[0]=0
            passed_time = np.cumsum(np.ediff1d(latencies))[data["index"] - 1]
            current_delay = np.maximum(passed_time - data["timestamp (s)"] + 1e-4,0)
            corr_time = np.minimum( current_delay, data["future inter request time (s)"]) #upper limit of actual irt
            #print(f'current irt: {np.ediff1d(latencies)[data["index"] - 1]} /current delay: {current_delay} / corr_time: {corr_time} / waited time: {data["future inter request time (s)"] - corr_time}')
            time.sleep(data["future inter request time (s)"] - corr_time)
            # time.sleep(data[3]) #vanilla sleep
        self.index = self.q.pop()
        self.data = data = self.dataset.iloc[self.index]
        latencies[self.index] = time.perf_counter()
        #print(str(self.ID) + ": " + str(self.index))
        #print(f"timestamp: {time.perf_counter()}")
        queueLock.release()
        delay = self.environment._step(1,self.data) if self.track_ssd_feature else self.placement_s(data)
        ssd_delays[self.index]=delay
        #irt = predict_inter_request_time(self)
        #inter_request_time[self.index] = irt

def train_step(model, loss_fn, optimizer, train_acc_metric, X, Y):
    with tf.GradientTape() as tape:
        logits = model(X, training=True)
        loss_value = loss_fn(Y, logits)
    grads = tape.gradient(loss_value, model.trainable_weights)
    optimizer.apply_gradients(zip(grads, model.trainable_weights))
    train_acc_metric.update_state(Y, logits)
    return loss_value

def train(model, X, Y):
    if len(Y) < 32:
        return
    optimizer = model.optimizer
    loss_fn = model.loss
    train_acc_metric = model.metrics[1]
    #X_batch_wise = X[-32*(len(X)//32):].reshape(-1,32,X.shape[1],X.shape[2]) #discard the oldest entries, such that the nof request is divisible by the batchsize
    #Y_batch_wise = Y[-32*(len(X)//32):].reshape(-1,32,Y.shape[1])
    for step, (x_batch_train, y_batch_train) in enumerate(zip(X,Y)):
        x_batch_train=x_batch_train.reshape(1, x_batch_train.shape[0], x_batch_train.shape[1])
        y_batch_train=y_batch_train.reshape(1, y_batch_train.shape[0])
        loss_value = train_step(model, loss_fn, optimizer, train_acc_metric,
                                x_batch_train, y_batch_train)

        print("Training loss (for one batch) at step %d: %.4f" % (step, float(loss_value)))


def main(args):
    model_name = "lstm"
    use_tflite = False
    global latencies, ssd_delays, inter_request_time
    s, study_args, study_params, trial_nr = load_environment(args, model_name, use_tflite)
    df_data, df, X, Y, scaler = load_workload(study_args, study_params["n_past"],s["df_part"], model_name)
    model = load_model(s, study_params, model_name)
    #create folder for evaluation results within the folder of the trained model
    eval_folder_path = Path("artifacts","eval"+str(int(time.time_ns() / 1e9)))
    os.mkdir(eval_folder_path)
    #model.save(Path(eval_folder_path, "model_pretrain.h5"))

    df_data = df_data[study_params["n_past"] - 1:-1] #delete first n_paast entries from dataset

    # do online training and evaluate result model, test, X, Y, scaler, n_past, online_train_interval, log_irt,
    #test_true, test_pred, model_evaluated, loss_online = evaluate_online(model, df, X, Y, scaler, study_params["n_past"],
    #              study_params["online_train_interval"], study_args.log_irt)
    #model_evaluated.save(Path(eval_folder_path))
    #plot_predictions_detailed(test_true, test_pred, eval_folder_path, description = "Online_evaluation_unscaled_test", show_plot=False)

    #initialize global variables
    df_len = len(df_data)
    latencies = np.zeros(df_len)
    ssd_delays = np.zeros(df_len)
    inter_request_time = np.zeros(df_len)

    #initialize ssd environment
    if args.track_ssd_feature:
        environment = HybridStorageEnvironment(HybridStorage(args.so_path, args.sim, df_data))
        workQueue = list(reversed(range(environment._hybrid._trace_length)))
    else:
        environment = HybridStorage(args.so_path, args.sim, df_data)
        workQueue = list(reversed(range(environment._trace_length)))

    if not args.sim: #TRIM
        gLBAFast = 1024 * 1024 * 1024
        command="sudo blkdiscard -o " + str(gLBAFast) + " -l 104857600 /dev/nvme0n1"
        er = os.system(command)
        if er == -1:
           raise("Error TRIM")
        print("TRIM finished")
        time.sleep(10)

    threads = []
    nof_threads = args.nof_threads
    print(f"NUmber of threads: {nof_threads}")

    lock = threading.Lock()
    queueLock.acquire()
    # Create new threads
    for ID in range(nof_threads):
        cloned_model = keras.models.clone_model(model)
        cloned_model.set_weights(model.get_weights())
        thread = requestThread(ID, workQueue, environment, cloned_model, [X,Y], args.track_ssd_feature,
                               args.sim, args.so_path, use_tflite, scaler=scaler, log_irt=True, modelLock=lock)
        thread.start()
        threads.append(thread)
    queueLock.release()

    #do training while the reuqest threads are working
    q_val_old = 0
    if args.train_online:
        while any(thread.is_alive() for thread in threads):
            time.sleep(1)
            queueLock.acquire()
            q_val = len(X) - len(workQueue)
            queueLock.release()
            if q_val-q_val_old < 1000:
                continue
            q_val_old = max(q_val_old,q_val-32) #take the recent 32 requests
            train(model, X[q_val_old:q_val], Y[q_val_old:q_val])
            q_val_old = q_val
            for thread in threads:
                with thread.modelLock:
                    thread.model.set_weights(model.get_weights())
            print("weights updated")
    else:
        # Wait for all threads to complete
        for t in threads:
            t.join()
    print(np.mean(np.ediff1d(latencies)))
    #df["SSD processing time (s)"] = ssd_delays[:len(df)] * 1e-9
    #dest_path = Path(args.workload_path[:-4]+"_it.csv")
    #df.to_csv(dest_path, index=False)
    #print(f"Dest. path: {dest_path}")


    fig, ax = plt.subplots(1, 1)
    plt.yscale('log')
    irt = df_data["future inter request time (s)"].to_numpy()

    ax.plot(irt[200:280], label="inter request time")
    #plt.plot(np.ediff1d()[200:280], label="python delay LSTM")
    plt.plot(np.ediff1d(latencies)[200:280], label="python delay")
    ax.plot(ssd_delays[200:280], label="ssd delay")

    # plt.legend(loc='upper right')
    plt.xlabel("Request index (on unit)")
    plt.ylabel("Delay/Time (s)")
    plt.legend()
    plt.show()
    if True:
        y_true = np.exp(df.iloc[study_params["n_past"]-1:-1]['future inter request time (s)'].to_numpy())
    else:
        y_true = test.iloc[study_params["n_past"]-1:-1]['future inter request time (s)'].to_numpy()

    assert len(y_true) == len(inter_request_time)
    rmse = np.sqrt(mse(y_true, inter_request_time))
    print(f"RMSE: {rmse}")
    loss = custom_loss(12) #penatly factor for underprediction is 12
    print(f"custom loss: {loss(y_true, inter_request_time)}")
    #print(f"Mean inference time: {sum(inf_time)/len(inf_time)}")
    print(eval_folder_path)
    plot_predictions_detailed(y_true=y_true, y_pred=inter_request_time, model_path=eval_folder_path,
                              description="Multithread prediction", show_plot=True)
    path_lat = Path(eval_folder_path,"timestamps_mt_serving_delay.npy")
    path_ssd_del = Path(eval_folder_path, "ssd_delay.npy")
    path_irt = Path(eval_folder_path, "irt.npy")
    np.save(path_lat, np.array(latencies))
    np.save(path_ssd_del, np.array(ssd_delays))
    np.save(path_irt, np.array(irt))

def argparser():
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        add_help=False
    )
    parser.add_argument("so_path")
    parser.add_argument("nof_threads", type=int)
    parser.add_argument("--sim", action='store_true')
    parser.add_argument("--track_ssd_feature", action="store_true")
    parser.add_argument("--train_online", action="store_true")
    parser.add_argument("-i", "--workload_item", required=True)
    parser.add_argument("-w", "--workload", required=True)
    return parser


if __name__ == '__main__':
    parser = argparser()
    args = parser.parse_args()

    main(args)
