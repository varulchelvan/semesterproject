import numpy as np
import pandas as pd
from sklearn.preprocessing import MinMaxScaler, StandardScaler, QuantileTransformer
from pathlib import Path
import json
from keras.utils import to_categorical


def get_time_series_shape2(*args):
    dataset = args[0][0]
    size = args[0][1]
    n_past = args[0][2]
    n_future = args[0][3]
    # separate trainfeatures and testfeatures
    presetX = dataset.drop(["future inter request time (s)"], axis=1).values
    presetY = dataset.iloc[:]["future inter request time (s)"].values

    # scaling
    if len(args[0]) == 4:
        scalerX = MinMaxScaler(feature_range=(-1, 1))  # QuantileTransformer()
        scalerY = MinMaxScaler(feature_range=(-1, 1))  # QuantileTransformer()
        scalerX = scalerX.fit(presetX)
        scalerY = scalerY.fit(presetY.reshape(-1, 1))
    else:
        scalerX = args[0][5]
        scalerY = args[0][6]

    presetX = scalerX.transform(presetX)
    presetY = scalerY.transform(presetY.reshape(-1, 1))

    # As required for LSTM networks,  reshape an input data into n_samples x timesteps x n_features.
    # Empty lists to be populated using formatted training data
    X = []
    Y = []

    # Reformat input data into a shape: (n_samples x timesteps x n_features)
    for i in range(n_past, size - n_future + 1):
        X.append(presetX[i - n_past:i, :])
        Y.append(presetY[i - 1:i, 0])

    X, Y = np.array(X), np.array(Y)

    print('X shape == {}.'.format(X.shape))
    print('Y shape == {}.'.format(Y.shape))
    return X, Y, scalerX, scalerY

def inverse_scale(scaler, data):
    #data = data.reshape(-1,1)
    prediction_copies = np.repeat(data, scaler.data_range_.shape[0], axis=-1)
    return scaler.inverse_transform(prediction_copies)[:, -1]

def get_time_series_shape1(*args):
    # either the trainset or the test is given as input.
    # given the trainset, the scaler is generated.
    # given the testset, the scaler is an additional input

    dataset = args[0][0]
    size = args[0][1]
    n_past = args[0][2]
    n_future = args[0][3]

    # scaling
    if len(args[0]) == 4:
        scaler = MinMaxScaler(feature_range=(-1, 1))  # QuantileTransformer()
        scaler = scaler.fit(dataset.values)
    else:
        scaler = args[0][5]

    preset = scaler.transform(dataset)

    # As required for LSTM networks,  reshape an input data into n_samples x timesteps x n_features.
    # Empty lists to be populated using formatted training data
    X = []
    Y = []

    # Reformat input data into a shape: (n_samples x timesteps x n_features)
    for i in range(n_past, size - n_future + 1):
        X.append(preset[i - n_past:i, :-1])
        Y.append(preset[i - 1:i, -1])

    X, Y = np.array(X), np.array(Y)

    print('X shape == {}.'.format(X.shape))
    print('Y shape == {}.'.format(Y.shape))
    return X, Y, scaler

def df_get_one_hot(df, col_name, prefix):
    nof_cols= df.shape[1]
    nof_new_cols = len(df[col_name].unique())
    df_op = pd.get_dummies(df[col_name], prefix=prefix, dtype=int)
    df.drop([col_name], axis=1, inplace=True)
    df = pd.concat([df_op, df], axis=1)
    assert nof_cols + nof_new_cols -1 == df.shape[1]
    return df

def preprocess(df, settings, log_irt, n_past, df_part, workload):
    # Choose features for training
    if "ycsb" in str(workload):
        cols = settings["df_shape"]["features_ycsb_exclude"]
    else:
        cols = settings["df_shape"]["features_msr_cambridge_exclude"]

    if "ProcessID" in cols:
        pid_columns = [col for col in df.columns if col.startswith("PID_")]
        df = df.drop(pid_columns, axis=1)

    if "CPU Core ID" in cols:
        cpu_columns = [col for col in df.columns if col.startswith("CPU_")]
        df = df.drop(cpu_columns, axis=1)

    #print(str(df_mod.columns))
    irt_future = df.iloc[1:]["inter request time (s)"]
    irt_future.reset_index(drop=True, inplace=True)
    df = df.head(-2)
    df.reset_index(drop=True, inplace=True)
    df["future inter request time (s)"] = irt_future

    df_mod = df.copy()

    #Preprocess workload for threads (df)
    df.drop(["SSD processing time (s)"], axis=1, inplace=True)
    try:
        df.drop(["idle time (s)"], axis=1, inplace=True)
    except:
        pass
    df.replace("W", 1, inplace=True)
    df.replace("WM", 1, inplace=True)
    df.replace("R", 0, inplace=True)
    df.replace("RSM", 0, inplace=True)

    #limit df
    df = df[df_part[0]:min(len(df_mod), df_part[1])]

    #Preprocess data for model (df_mod)
    for col in cols:
        try:
            df_mod.drop([col], axis=1, inplace=True)
        except:
            pass

    # replace read and write with integer numbers
    df_mod.replace("W", "Write", inplace=True)
    df_mod.replace("WM", "Write", inplace=True)
    df_mod.replace("R", "Read", inplace=True)
    df_mod.replace("RSM", "Read", inplace=True)

    # substitute neg SSD Processing time
    if "SSD processing time (s)" in str(cols):
        neg_ssd_time_idx = df_mod[df_mod["SSD processing time (s)"] < 0].index.values
        df_mod_R = df_mod[df_mod["OperationType"] == "Write"]
        df_mod_W = df_mod[df_mod["OperationType"] == "Read"]

        for i in neg_ssd_time_idx:
            if df_mod.iloc[i]["OperationType"] == 0:
                t = df_mod_R[df_mod_R.index < i].iloc[-1]["SSD processing time (s)"]
            else:
                t = df_mod_W[df_mod_W.index < i].iloc[-1]["SSD processing time (s)"]
            df_mod.loc[i, "SSD processing time (s)"] = t

    #One Hot encoding of categorical variables
    df_mod = df_get_one_hot(df_mod, col_name="OperationType", prefix="Op_") #

    # Reorder
    # cols = list(df_mod.columns)
    # cols.remove("future inter request time (s)")
    # cols.remove("inter request time (s)")
    # cols.extend(["inter request time (s)", "future inter request time (s)"])
    # df_mod = df_mod[cols]

    if log_irt:  # if the log is taken from irt, all entries with 0 must be replaced
        df_mod["inter request time (s)"].replace(0, 1e-9, inplace=True)
        df_mod["future inter request time (s)"].replace(0, 1e-9, inplace=True)
        df_mod['future inter request time (s)'] = np.log(df_mod['future inter request time (s)'])
        df_mod['inter request time (s)'] = np.log(df_mod['inter request time (s)'])

    # check if future idle time is right
    for i in np.random.randint(0, len(df_mod) - 1, size=10):
        assert df_mod.iloc[i]["future inter request time (s)"] == df_mod.iloc[i + 1]["inter request time (s)"]

    #check if target label is the last column
    assert df_mod.columns[-1] == "future inter request time (s)"

    #limit length of dataset
    df_mod = df_mod[df_part[0]:min(len(df_mod), df_part[1])]


    # split into train and test sets
    n_future = settings["df_shape"]["n_future"]  # Number of timestamps to look into the future based on the past timestamps.
    train_size = (int(len(df_mod) - n_past) //10) *10 + n_past  # make sure the train_size is divisible by the batchsize even after validation split
    train = df_mod[0:train_size]
    print(f" Data set size: {train_size} \n")

    # feature and target label generation
    trainX, trainY, scaler = get_time_series_shape1([train, train_size, n_past, n_future])

    # Xshape = [setsize,npast,featureset]
    # Yshape = [setsize,1]
    # check if the featureset from the current timestamp is in the featureset of the next timestamp on the index "index" before,
    # since this featureset becomes older (older featureset are more on top than newer ones)
    assert np.array_equal(trainX[2, 2], trainX[3, 1])
    assert np.array_equal(trainX[-2, 2], trainX[-1, 1])
    # check if the prediction of one timestamp is in the featureset of the next timestamp on the bottom.
    irt_idx = df_mod.columns.get_loc("inter request time (s)")
    assert np.array_equal(trainY[3][0], trainX[4,-1,irt_idx])
    assert np.array_equal(trainY[-2][0], trainX[-1,-1,irt_idx])

    # check if transformation is made right (check first 20 entries)
    if log_irt:
        assert np.equal(np.round(np.exp(inverse_scale(scaler, trainY))[1:20], 13),
                        np.round(np.exp(train.iloc[n_past-1:]["future inter request time (s)"]).values[1:20], 13)).all()
    else:
        assert np.equal(np.round(inverse_scale(scaler, trainY)[1:20], 13),
                        np.round(train.iloc[n_past-1:]["future inter request time (s)"].values[1:20], 13)).all()
    
    try:
        if settings["hyperparam"]["tcn"]["classify"] == 1:
            print("### Classifier ####")
            trainY = to_categorical(trainY, len(train["future inter request time (s)"].unique()))
    except:
        pass

    return df, train, trainX, trainY, scaler


def load_workload(args, n_past, df_part, model_name):
    with open(Path("model_data",model_name,"params.json")) as f:
        s = json.load(f)

    path= Path(s["paths"]["df_prefix_loc"])
    path=Path(path,s["paths"][args.workload][args.workload_item])

    df = pd.read_csv(Path(path))
    df_data, df, X, Y, scaler = preprocess(df, s, args.log_irt, n_past, df_part, args.workload)
    return df_data, df, X, Y, scaler
