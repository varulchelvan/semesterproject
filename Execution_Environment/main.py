from model import *
from pathlib import Path
from hybridstorage import HybridStorage
from hybridstorageenvironment import HybridStorageEnvironment
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

import os
import threading

from model import predict_online_lite, predict_online
from ssd_sim import *
from ctypes import *

queueLock = threading.Lock() #lock for request threads
workQueue = None #buffer containing request indices
latencies = np.zeros(1) #dummy initializations
ssd_delays = np.zeros(1)
inter_request_time = np.zeros(1)
queue_empty = False

class requestThread(threading.Thread):
    def __init__(self, threadID, q, environment, model, data_scaled, track_ssd_feature,
                 sim, so_path, use_tflite, scaler, log_irt, modelLock):
        X,Y = data_scaled
        threading.Thread.__init__(self)
        self.ID = threadID
        self.q = q
        self.modelLock = modelLock
        self.data = None
        self.index = None
        self.environment = environment
        self.model = model
        self.X = X
        self.Y = Y
        self.scaler = scaler
        self.log_irt = log_irt
        self.use_tflite = use_tflite
        self.my_functions = ssd_sim() if sim else CDLL(so_path)
        self.fastDevice = self.my_functions.openFastDevice()
        #self.slowDevice = self.my_functions.openSlowDevice()
        self.track_ssd_feature = track_ssd_feature
        self.dataset = self.environment._hybrid._df if self.track_ssd_feature else self.environment._df

    def run(self):
            time.sleep(2)
            process_data(self)

    def placement_s(self, data):  # shrinked version of placement
        if (data[2] == 1):
            lat = self._reqLatency = self.my_functions.sibyl_write(self.fastDevice, str(data[0]), int(data[1]))
        else:
            lat = self.my_functions.sibyl_read(self.fastDevice, str(data[0]), int(data[1]))
        self._final_latency = lat
        return self._final_latency

def predict_inter_request_time(thread):
    idx = thread.data["index"]
    if thread.use_tflite:
        irt = predict_online_lite(thread.modelLock, thread.model, thread.X[idx:idx + 1], thread.scaler, thread.log_irt)
    else:
        irt = predict_online(thread.modelLock, thread.model, thread.X[thread.data["index"]], thread.scaler, thread.log_irt) #model, X, scaler, log_irt
    return irt

def get_idle_time(irt):
    passed_time = np.cumsum(np.ediff1d(latencies))[data["index"] - 1]
    current_delay = np.maximum(passed_time - data["timestamp (s)"] + 1e-4, 0) #add 1us processing delay
    return max(0, irt-current_delay)

def process_data(thread):
    global latencies
    global data
    data = thread.dataset.iloc[0]
    data["future inter request time (s)"] = 0
    last_req = len(thread.environment._hybrid._df) if thread.track_ssd_feature else len(thread.environment._df)

    while True:
        queueLock.acquire()
        if data["index"] == last_req-1: #check if last request is arrived
            queueLock.release()
            return
        if data["future inter request time (s)"] > 1e-3:  # sleep with control loop
            passed_time = np.cumsum(np.ediff1d(latencies))[data["index"] - 1]
            current_delay = np.maximum(passed_time - data["timestamp (s)"] + 1e-4,0)
            corr_time = np.minimum( current_delay, data["future inter request time (s)"]) #upper limit of actual irt
            #print(f'current irt: {np.ediff1d(latencies)[data["index"] - 1]} /current delay: {current_delay} / corr_time: {corr_time} / waited time: {data["future inter request time (s)"] - corr_time}')
            time.sleep(data["future inter request time (s)"] - corr_time)
            # time.sleep(data[3]) #vanilla sleep
        thread.index = thread.q.pop()
        thread.data = data = thread.dataset.iloc[thread.index]
        latencies[thread.index] = time.perf_counter()
        #print(str(self.ID) + ": " + str(self.index))
        #print(f"timestamp: {time.perf_counter()}")
        queueLock.release()
        delay = thread.environment._step(1,thread.data) if thread.track_ssd_feature else thread.placement_s(data)
        ssd_delays[thread.index]=delay
        irt = predict_inter_request_time(thread)
        inter_request_time[thread.index] = irt
        get_idle_time(irt)

########################################################################################################################

def main(args):
    #define global variables
    global latencies, ssd_delays, inter_request_time, queueLock, requestThread
    threads = [] #list of threads : If tflite only one item in this list, multiple threads will generated within this item
    nof_threads = args.nof_threads
    use_tflite = args.tflite

    if use_tflite:
        model_name = "tflite_tcn_final"
    else:
        model_name = "tcn"
    print(f"Number of request threads: {nof_threads}")

    s, study_args, study_params, trial_nr = load_environment(args, model_name, use_tflite) #load environment not all settings from params.json are used in exec. environmnet
    df_data, df, X, Y, scaler = load_workload(study_args, study_params["n_past"],s["df_part"], model_name) #load data acording to the given settings


    #load offline trained model
    if use_tflite:
        X = X.astype("float32")
        Y = Y.astype("float32")
    else:
        model = load_model(s, study_params, model_name)


    #create folder for evaluation results within the folder of the trained model
    eval_folder_path = Path("artifacts","eval"+str(int(time.time_ns() / 1e9)))
    os.mkdir(eval_folder_path)
    df_data = df_data[study_params["n_past"] - 1:-1] #delete first n_past entries from dataset


    #initialize global variables
    df_len = len(df_data)
    latencies = np.zeros(df_len)
    ssd_delays = np.zeros(df_len)
    inter_request_time = np.zeros(df_len)


    #initialize ssd environment
    if args.track_ssd_feature:
        environment = HybridStorageEnvironment(HybridStorage(args.so_path, args.sim, df_data))
        workQueue = list(reversed(range(environment._hybrid._trace_length)))
    else:
        environment = HybridStorage(args.so_path, args.sim, df_data)
        workQueue = list(reversed(range(environment._trace_length)))

    if not args.sim: #TRIM
        gLBAFast = 1024 * 1024 * 1024
        command="sudo blkdiscard -o " + str(gLBAFast) + " -l 104857600 /dev/nvme0n1"
        er = os.system(command)
        if er == -1:
           raise("Error TRIM")
        print("TRIM finished")
        time.sleep(10)
    lock = threading.Lock()


    #create request threads
    if use_tflite:
        queueLock.acquire()
        for ID in range(nof_threads):
            interpreter, model = load_lite_model(X, Y, nofthreads=1, model_name=model_name)
            thread = requestThread(ID, workQueue, environment, interpreter, [X, Y], args.track_ssd_feature,
                                   args.sim, args.so_path, use_tflite, scaler=scaler, log_irt=study_args.log_irt, modelLock=lock)
            threads.append(thread)
            thread.start()
        queueLock.release()
    else:
        queueLock.acquire()
        for ID in range(nof_threads):
            cloned_model = keras.models.clone_model(model)
            cloned_model.set_weights(model.get_weights())
            thread = requestThread(ID, workQueue, environment, cloned_model, [X,Y], args.track_ssd_feature,
                                   args.sim, args.so_path, use_tflite, scaler=scaler, log_irt=study_args.log_irt, modelLock=lock)
            thread.start()
            threads.append(thread)
        queueLock.release()


    #do training while the reuqest threads are working
    if use_tflite:
        train_interpreter, train_model = load_lite_model(X, Y, nofthreads=1, model_name=model_name)
    q_val_old = 0
    if args.train_online:
        while any(thread.is_alive() for thread in threads):
            time.sleep(1)
            queueLock.acquire()
            q_val = len(X) - len(workQueue)
            queueLock.release()
            if q_val-q_val_old < 2000:
                continue
            q_val_old = max(q_val_old,q_val-100) #take the recent 100 requests
            if use_tflite:
                train_lite(train_interpreter, X[q_val_old:q_val], Y[q_val_old:q_val])
                q_val_old = q_val
                cloned_interpreter = tf.lite.Interpreter(model_content=train_model)
                with threads[0].modelLock:
                    threads[0].model = cloned_interpreter
                    threads[0].model.allocate_tensors()
            else:
                train(model, X[q_val_old:q_val], Y[q_val_old:q_val])
                q_val_old = q_val
                for thread in threads:
                    with thread.modelLock:
                        thread.model.set_weights(model.get_weights())

    # Wait for all threads to complete
    for t in threads:
        t.join()


    #Plot results
    fig, ax = plt.subplots(1, 1)
    plt.yscale('log')
    irt = df_data["future inter request time (s)"].to_numpy()

    ax.plot(irt[1000:2000], label="inter request time")
    plt.plot(inf_time[1000:2000], label="Inference time")
    plt.plot(np.ediff1d(latencies)[1000:2000], label="python delay")
    ax.plot(ssd_delays[1000:2000]* 1e-9, label="ssd delay")
    plt.xlabel("Request index (on unit)")
    plt.ylabel("Delay/Time (s)")
    plt.legend()
    plt.savefig(Path(eval_folder_path,"timings"))
    if study_args.log_irt:
        y_true = np.exp(df.iloc[study_params["n_past"]-1:-1]['future inter request time (s)'].to_numpy())
    else:
        y_true = df.iloc[study_params["n_past"]-1:-1]['future inter request time (s)'].to_numpy()

    assert len(y_true) == len(inter_request_time)
    rmse = np.sqrt(mse(y_true, inter_request_time))

    print(f"###########################################################################")
    print(f"RMSE: {rmse}")
    loss = custom_loss(12) #penalty factor for underprediction is 12
    print(f"Custom Loss: {loss(y_true, inter_request_time)}")
    print(f"Mean Inference Time: {sum(inf_time[2001:])/(len(inf_time[2001:]))}")
    inf_time.sort(reverse=True)
    print(f"90% of the request take less time than: {inf_time[int(len(inf_time) * 0.1):][0]}")
    print(f"99% of the request take less time than: {inf_time[int(len(inf_time) * 0.01):][0]}")
    print(f"Eval folder: {eval_folder_path}")
    print(f"###########################################################################")

    plot_predictions_detailed(y_true=y_true, y_pred=inter_request_time, model_path=eval_folder_path,
                              description="Multithread prediction", show_plot=True)


    #Save Results
    path_lat = Path(eval_folder_path,"timestamps_mt_serving_delay.npy")
    path_ssd_del = Path(eval_folder_path, "ssd_delay.npy")
    path_inference_del = Path(eval_folder_path, "inference.npy")
    path_irt = Path(eval_folder_path, "irt.npy")
    np.save(path_lat, np.array(latencies))
    np.save(path_ssd_del, np.array(ssd_delays))
    np.save(path_irt, np.array(irt))
    np.save(path_inference_del, np.array(inf_time))
    return

def argparser():
    parser = ArgumentParser(
        formatter_class=ArgumentDefaultsHelpFormatter,
        add_help=False
    )
    parser.add_argument("so_path")
    parser.add_argument("nof_threads", type=int)
    parser.add_argument("--sim", action='store_true')
    parser.add_argument("--track_ssd_feature", action="store_true")
    parser.add_argument("--train_online", action="store_true")
    parser.add_argument("-i", "--workload_item", required=True)
    parser.add_argument("-w", "--workload", required=True)
    parser.add_argument("--tflite", action="store_true")
    return parser


if __name__ == '__main__':
    parser = argparser()
    args = parser.parse_args()
    main(args)
