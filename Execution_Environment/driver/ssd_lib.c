#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <time.h>
#include <sys/time.h>
//fast = nvme0n1

int openFastDevice()
{
	int fp = open("/dev/nvme0n1", O_RDWR|O_SYNC);
    printf("FP=%d\n", fp);
    if(fp <= 0) {
        perror("Error opening file");
        return(-1);
    }
	return fp;
}

int openMiddleDevice()
{
	int fp = open("/dev/sdb", O_RDWR|O_SYNC);
	fp = fopen("/tmp/test.txt", "w");
    printf("FP=%d\n", fp);
    if(fp <= 0) {
        perror("Error opening file");
        return(-1);
    }
	return fp;
}

int openSlowDevice()
{
	int fp = open("/dev/sda3", O_RDWR|O_SYNC);
    printf("FP=%d\n", fp);
    if(fp <= 0) {
        perror("Error opening file");
        return(-1);
    }
	return fp;
}

int sibyl_read(int fd, unsigned long byte_offset, unsigned int nSize)
{
    struct timespec stop, start;
	char readBuf[nSize] __attribute__ ((__aligned__ (4096))); //create buffer with 4096-bytes (not the ssd itself)
	memset(readBuf, 0x00, sizeof(char) * nSize); //initialize all entries with 0
	off_t readOffset = lseek(fd, byte_offset, SEEK_SET); //set the offset(index within the ssd) according to the given byte offset
	clock_gettime( CLOCK_MONOTONIC_RAW, &start);
	//gettimeofday(&start,NULL);
	ssize_t len = read(fd, readBuf, sizeof(char) * nSize); //read the content with size nSize from the device where the fp points to.
	//usleep(1000);
	clock_gettime( CLOCK_MONOTONIC_RAW, &stop);
	//gettimeofday(&stop,NULL);
	return (double)(stop.tv_nsec - start.tv_nsec);
}

int sibyl_write(int fd, unsigned long byte_offset, unsigned int nSize)
{
	struct timespec stop, start;
	char writeBuf[nSize] __attribute__ ((__aligned__ (4096)));
	memset(writeBuf, 0xA5, sizeof(char) * nSize);   //fill the buffer with 0xA5 (content to write on the ssd)
	off_t writeOffset = lseek(fd, byte_offset, SEEK_SET); //set offset within the ssd
	clock_gettime( CLOCK_MONOTONIC_RAW, &start);
	//gettimeofday(&stop,NULL);
	ssize_t len = write(fd, writeBuf, sizeof(char) * nSize);
	//usleep(1000);
	clock_gettime( CLOCK_MONOTONIC_RAW, &stop);
	//gettimeofday(&stop,NULL);
	return (double)(stop.tv_nsec - start.tv_nsec);
}

void closeDevice(int fd)
{
	close(fd);
}
