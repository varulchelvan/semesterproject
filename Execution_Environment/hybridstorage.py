import pandas as pd
import numpy as np
# import time
# import os
# import sys
# from ctypes import *
from ssd_sim import *
from ctypes import *

import logging


class HybridStorage():
    def __init__(self, so_path, sim, workload):
        self.my_functions = ssd_sim() if sim else CDLL(so_path)
        self.fastDevice = self.my_functions.openFastDevice()
        #self.slowDevice = self.my_functions.openSlowDevice()
        self.gLBAFast = 1024 * 1024 * 1024
        self.gLBASlow = 1024 * 1024 * 1024
        # if application.find("MSR-Cambridge") > 0:  # if the workload is from MSR-cambridge
        #     cols = [0, 1, 2, 3, 5]  # msr-cambridge
        #     col_name = ['timestamp (s)', 'operation type', 'offset', 'size (bytes)', 'inter request time (s)']
        #
        # elif application.find("atto-qd") > 0:
        #     cols = [0, 1, 2, 3, 4, 5, 7]  # atto-qd-2
        #     col_name = ['operation type', 'offset', 'size (bytes)']
        #
        # elif application.find("YCSB") > 0:
        #     cols = [0, 1, 2, 3, 4, 5, 6]
        #     col_name = ['CPU core ID', 'timestamp (s)', 'process ID', 'operation type', 'offset', 'size (bytes)', 'inter request time (s)']
        #
        # else:
        #     cols = [3, 4, 5]
        #     col_name = ['operation type', 'offset', 'size (bytes)']
        # input_file = self.application
        # workload = pd.read_csv(input_file, usecols=cols)
        # workload.columns = col_name
        # self.trace_file = workload[['offset', 'size (bytes)', 'operation type', 'inter request time (s)', 'timestamp (s)']]
        # if application.find("YCSB") > 0:
        #     self.appl_features = workload[['CPU core ID', 'timestamp (s)', 'process ID', 'operation type', 'offset', 'size (bytes)', 'inter request time (s)']]
        # self.trace_file["operation type"].replace("W", 1, inplace=True)
        # self.trace_file["operation type"].replace("WM", 1, inplace=True)
        # self.trace_file["operation type"].replace("R", 0, inplace=True)
        # self.trace_file["operation type"].replace("RSM", 0, inplace=True)
        self.trace_file = workload
        self.trace_file["index"] = np.array(range(len(self.trace_file)))
        self._trace_length = len(self.trace_file.index)
        self._trace_shape = self.trace_file.shape
        self._df = self.trace_file
        # self.trace_file = self.trace_file.to_numpy()
        self._delta_timestamp = workload["inter request time (s)"]
        self._df['SectorNumber'] = self._df['SectorNumber'].astype("str")
        self._df['size (bytes)'] = self._df['size (bytes)'].astype("int")
        self._df['OperationType'] = self._df['OperationType'].astype("int8")
        self._idle_time = np.zeros([len(workload), 1])  # goal is to take the average idle_time over multiple episods
        self.acc_time = 0  # remaining SSD processing time after the current request has been processed (request is not done by the SSD yet!)
        self._devices = pd.DataFrame(columns=["Device", "Capacity"])
        self._devices["Filled"] = 0
        self._devices["WriteCount"] = 0
        self._devices["ReadCount"] = 0
        self._devices.set_index('Device', inplace=True)
        self._devices.at['fastSSD', 'Capacity'] = (2 * 1024 * 1024 * 1024)
        self._devices.at["slowSSD", "Capacity"] = (1.76 * 1024 * 1024 * 1024 * 1024)
        self._devices.at["fastSSD", "Filled"] = self._devices.at["slowSSD", "Filled"] = 0
        self._devices.at["fastSSD", "WriteCount"] = self._devices.at["slowSSD", "WriteCount"] = 0
        self._devices.at["fastSSD", "ReadCount"] = self._devices.at["slowSSD", "ReadCount"] = 0
        self.SSDFEvictionThreshold = 90
        self._trace_index = 0
        self.metadata_time = self.qrator_proc_time = self.exec_time = 0
        self.numEvicts = 0
        self._current_fast_capacity = self._fast_capacity = 50
        self._final_latency = self._reqLatency = self._evictLatency = 0
        self._migration = 0
        self._invalid_page_fast = self._invalid_page_slow = 0
        self._invalid_fast = 0
        self.size_max = self._df['size (bytes)'].max()
        self.size_min = self._df['size (bytes)'].min()
        self._mapping_table = pd.DataFrame(
            columns=["Size", "ReadWrite", "Device", "LBA", "LatestWriteCount", "LatestReadCount", \
                     "TotalWrites", "TotalReads", "NumMigrationsSSD1", "NumMigrationsSSD2", "PrevAction", "CurAction",
                     "ReuseDist"
                     ])
        self._metadata_table = pd.DataFrame(columns=["accessCount", "accessInterval", "Device"])
        self.all_addr_freq = {}
        # self._state = np.zeros((self._trace_shape), dtype=np.float64)
        self._state = self._df.values

        # observation variable
        self.nof_write_creation = 0  # counts nof write creation, which was done due to a read to a unwritten offset

    def reset(self):
        self._state = np.zeros((self._trace_shape), dtype=np.float64)
        self._state = self._df
        self.numEvicts = 0
        self._trace_index = 0
        self._current_fast_capacity = 0
        self._final_latency = self._reqLatency = self._evictLatency = 0
        self._invalid_page_fast = self._invalid_page_slow = 0
        self._mapping_table.iloc[0:0]
        self._metadata_table.iloc[0:0]
        self._invalid_fast = 0
        self._mapping_table.drop(self._mapping_table.index, inplace=True)
        self._metadata_table.drop(self._metadata_table.index, inplace=True)
        self._devices = pd.DataFrame(columns=["Device", "Capacity"])
        self._devices["Filled"] = self._devices["WriteCount"] = self._devices["ReadCount"] = 0
        self._devices.set_index('Device', inplace=True)
        self._devices.at['fastSSD', 'Capacity'] = (2 * 1024 * 1024 * 1024)
        self._devices.at["slowSSD", "Capacity"] = (
                1.76 * 1024 * 1024 * 1024 * 1024)  # 1.76 TB is the size of the slow SSD
        self._devices.at["fastSSD", "Filled"] = self._devices.at["slowSSD", "Filled"] = 0
        self._devices.at["fastSSD", "WriteCount"] = self._devices.at["slowSSD", "WriteCount"] = 0
        self._devices.at["fastSSD", "ReadCount"] = self._devices.at["slowSSD", "ReadCount"] = 0

    def read(self, obs, memory_type):
        latency = 0
        deviceName = ""
        request = []
        # request.append(str(obs[0][0]))  # VBA
        # request.append(int(obs[0][1]))  # ReadSize
        # request.append(int(obs[0][2]))
        request.extend([str(obs[0][0]), int(obs[0][1]), int(obs[0][2])])
        VBA = str(request[0])

        if VBA not in self._mapping_table.index:
            self.nof_write_creation += 1
            logging.debug(
                "write creation due to a read from a unwritten offset\n" + "Nof write creations: " + str(
                    self.nof_write_creation))
            self.write(obs, memory_type)

        if memory_type == 1:
            deviceName = "fastSSD"
        else:
            deviceName = "slowSSD"
        self._mapping_table.at[VBA, "TotalReads"] += 1
        self._mapping_table.at[VBA, "ReadWrite"] = 'Read'
        self._mapping_table.at[VBA, "ReuseDist"] = 0
        self._devices.at[deviceName, "ReadCount"] += 1
        self._mapping_table.at[VBA, "LatestReadCount"] += 1
        LBA = self._mapping_table.at[VBA, "LBA"]
        # Calculate latency
        # It is a sequential read, multiply sequential read latency by the number of chunks read

        #####################START
        dname = self._mapping_table.at[request[0], "Device"]  # current device

        # if (dname == "slowSSD") and (deviceName == "fastSSD"):
        #     logging.debug("\t\t\t[READ]****PROMOTE****")
        #     latency += self.promote(request, False)
        #
        # elif (dname == "fastSSD") and (deviceName == "slowSSD"):
        #     logging.debug("\t\t\t[READ] ****EVICT****")
        #     latency += self.evict(request, False)
        #
        # else:
        #     logging.debug("\t\t\t[READ] ****NO MOVE****")
        #####################END
        readSize = int(request[1])
        if deviceName == "slowSSD":
            start = time.perf_counter()
            self.my_functions.sibyl_read(self.slowDevice, LBA, readSize)
            end = time.perf_counter()

        if deviceName == "fastSSD":
            start = time.perf_counter()
            self.my_functions.sibyl_read(self.fastDevice, LBA, readSize)
            end = time.perf_counter()
        latency += (end - start)
        return latency

    def write(self, obs, memory_type):
        start = 0
        end = 0
        latency = 0
        createMapping = True
        newRequest = False  # use?
        request_metadata = []
        request = []
        request.extend([str(obs[0][0]), int(obs[0][1]), int(obs[0][2])])

        dname = ""
        deviceName = "fastSSD" if memory_type else "slowSSD"
        self._evictLatency = 0
        VBA = str(request[0])
        newSize = request[1]
        sizeToMove = newSize
        not_newRequest = False  # use?
        meta_access = meta_spatial = meta_burst = 0
        if request[0] in self._mapping_table.index:  # checking if the table already exists
            dname = self._mapping_table.at[request[0], "Device"]  # current device
            currSize = self._mapping_table.at[VBA, "Size"]
            oldLBA = self._mapping_table.at[VBA, "LBA"]
            writeCounter = self._mapping_table.at[request[0], "LatestWriteCount"] + 1
            readCounter = self._mapping_table.at[request[0], "LatestReadCount"]
            totalWriteCounter = self._mapping_table.at[request[0], "TotalWrites"] + 1
            totalReadCounter = self._mapping_table.at[request[0], "TotalReads"]
            numMigrations1 = self._mapping_table.at[request[0], "NumMigrationsSSD1"]
            numMigrations2 = self._mapping_table.at[request[0], "NumMigrationsSSD2"]
            prev_act = self._mapping_table.at[request[0], 'CurAction']
            self._mapping_table.at[request[0], 'PrevAction'] = prev_act
            self._mapping_table.at[request[0], 'CurAction'] = memory_type
            reuse = 0
            sizeToMove = newSize if newSize > currSize else currSize

            # if memory_type == 1:
            #     deviceName = "fastSSD"
            # else:
            #     deviceName = "slowSSD"

            if deviceName == "fastSSD":
                if newSize > currSize:
                    extraSize = newSize - currSize
                    filledPercent = 100 * (self._devices.at["fastSSD", "Filled"] + extraSize) / self._devices.at[
                        "fastSSD", "Capacity"]
                    if filledPercent > self.SSDFEvictionThreshold:
                        self._evictLatency = self.urgentEviction(extraSize)

            if dname != deviceName and dname != '':
                if dname == 'fastSSD':
                    self._invalid_page_fast = 1
                if dname == "slowSSD":
                    self._invalid_page_slow = 1
                    # Check if the new size is greater than currSize then creat a new mapping
            if newSize > currSize:
                self._devices.at[dname, "Filled"] -= currSize
                prev_reuse_dist = self._mapping_table.at[VBA, "ReuseDist"]
                self._mapping_table.drop(VBA, inplace=True)
                not_newRequest = True
            else:  # for currSize <= new Size; Just update the LBAs corresponding to the size and other metadata
                createMapping = False  # To not update the mapping table
                self._metadata_table.at[VBA, "Device"] = memory_type
                self._mapping_table.at[VBA, "LatestWriteCount"] = writeCounter
                self._mapping_table.at[VBA, "TotalWrites"] = totalWriteCounter
                self._mapping_table.at[VBA, "Device"] = deviceName
                self._mapping_table.at[VBA, "ReuseDist"] = 0
        # If it not yet in the mapping table, write to SSD1 unless SSD1 is already filled beyond threshold
        else:
            newRequest = True  # use?
            # if (memory_type == 1):
            #     deviceName = "fastSSD"
            # else:
            #     deviceName = "slowSSD"
            if deviceName == "fastSSD":
                filledPercent = 100 * (self._devices.at["fastSSD", "Filled"] + newSize) / self._devices.at[
                    "fastSSD", "Capacity"]
                if filledPercent > self.SSDFEvictionThreshold:
                    self._evictLatency = self.urgentEviction(newSize)
            writeCounter = totalWriteCounter = 1
            readCounter = totalReadCounter = 0
            numMigrations1 = numMigrations2 = 0
            reuse = 0
            meta_access = meta_spatial = 0
        # Calculate latency
        latency = 0
        if createMapping:  # Create new mapping
            if deviceName == "slowSSD":
                LBA = int(self.gLBASlow)
                self.gLBASlow += sizeToMove
            else:  # Fast SSD
                LBA = int(self.gLBAFast)
                self.gLBAFast += sizeToMove

            request_metadata.extend([meta_access, meta_spatial, memory_type])
            request.extend(
                [deviceName, LBA, writeCounter, readCounter, totalWriteCounter, totalReadCounter, numMigrations1,
                 numMigrations2, 1, memory_type, reuse])
            self._devices.at[deviceName, "Filled"] += sizeToMove
            self._mapping_table.loc[VBA] = request[1:]
            self._mapping_table.at[VBA, "Size"] = sizeToMove
            self._metadata_table.loc[VBA] = request_metadata

        else:  # No new mapping table entry is required
            LBA = self._mapping_table.at[VBA, "LBA"]
        self._mapping_table.at[VBA, "ReadWrite"] = 'Write'
        self._devices.at[deviceName, "WriteCount"] += 1
        # Calculate latency
        if deviceName == "fastSSD":
            start = time.perf_counter()
            self.my_functions.sibyl_write(self.fastDevice, LBA, newSize)
            end = time.perf_counter()
            latency = (end - start)
        else:
            start = time.perf_counter()
            self.my_functions.sibyl_write(self.slowDevice, LBA, newSize)
            end = time.perf_counter()
            latency = (end - start)
        return latency  # latency in s

    def placement(self, obs, action):
        self._mapping_table.ReuseDist += 1
        logging.debug("\t> Request type: {} (1: WRITE, 0: READ)".format(int(obs[0][2])))

        if (int(obs[0][2]) == 1):
            self._reqLatency = self.write(obs, action)
        else:
            self._reqLatency = self.read(obs, action)
        self._final_latency = self._reqLatency
        return self._reqLatency
