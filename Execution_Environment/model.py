from pathlib import Path
import json
from sklearn.metrics import mean_squared_error as mse
from argparse import Namespace
from data_loading import load_workload, inverse_scale
import numpy as np
import tensorflow as tf
from tensorflow import keras
from keras.metrics import RootMeanSquaredError
import time
import matplotlib.pyplot as plt
from tcn import TCN
from keras.layers import LSTM
from tqdm import tqdm

inf_time = []

def contains_cell(model, celltype):
    for layer in model.layers:
        if isinstance(layer, celltype):
            return True
    return False


def set_lr(lr, model):
    if "PiecewiseConstantDecay" in str(model.optimizer.lr):
        model.optimizer.lr.values = [lr] * len(model.optimizer.lr.values)
    else:
        model.optimizer.lr.assign(lr)

    return model

def custom_loss(alpha):
    def loss_fn(y_true, y_pred):
        beta = 1.0  # Weight for penalizing too low predictions
        error = y_pred - y_true
        loss = tf.where(error < 0, alpha * tf.abs(error), beta * tf.abs(error))
        return tf.reduce_mean(loss)

    return loss_fn

def load_environment(new_args, model_name, use_tflite):
    # open params.json of the project to get the folder path for the params.json of the trained model
    with open(Path("model_data", model_name,"params.json")) as f:
        s = json.load(f)
    # get input arguments of the trained model
    with open(Path("model_data", model_name,"args.json")) as f:
        args = json.load(f)
    args = Namespace(**args)
    args.workload = new_args.workload
    args.workload_item = new_args.workload_item

    # if the study.pkl exists, it will be used, otherwise some params are assumed
    try:
        t = Path("model_data", model_name, "study_params.json")
        with open(t) as f:
            study_params = json.load(f)
        trial_nr = 0
    except:
        print("Could not load study, taking trial 1, n_past=100")
        trial_nr = 0
        study_params = {}
        study_params["n_past"] = s["hyperparam"]["n_past"][0]
        study_params["online_train_interval"] = s["hyperparam"]["online_train_interval"][0]
        study_params["online_lr"] = 0.0001
        study_params["model_loss"] = "custom"
        study_params["beta"] = 12

    return s, args, study_params, trial_nr

def predict(model, X, Y, test, batch_size, start, end, n_past, log_irt, scaler):
    X = X[start:end]
    Y = Y[start:end]
    y_pred_scaled = model.predict(X, batch_size=batch_size, verbose=0).flatten()
    y_pred_scaled = y_pred_scaled.reshape(-1, 1)

    loss = model.evaluate(X, Y, verbose=0)
    if log_irt:
        y_pred = np.exp(inverse_scale(scaler, y_pred_scaled))
        if model.layers[-1].output_shape[-1] == 1:
            y_pred = [min(y, 4) for y in y_pred] # set max irt 4
            y_pred = np.array(y_pred)
        y_check = np.exp(inverse_scale(scaler, Y))
        y_true = np.exp(test.iloc[n_past-1:-1]['future inter request time (s)'].to_numpy())
    else:
        y_pred = inverse_scale(scaler, y_pred_scaled)
        y_check = inverse_scale(scaler, Y)
        y_true = test.iloc[n_past-1:-1]['future inter request time (s)'].to_numpy()
    y_true = y_true[start:end]

    assert np.isclose(y_true,y_check, atol=1e-5).all() # check if y_true is really the target array
    assert len(y_pred) == len(X)
    rmse = np.sqrt(mse(y_true, y_pred))
    return y_true, y_pred, y_pred_scaled, [rmse, loss[0]]

def plot_predictions_detailed(y_true, y_pred, model_path, description, show_plot):
    assert len(y_true)==len(y_pred)
    print("Plot " + description.replace("_", " "))
    plt.yscale('log')
    plt.plot(abs(y_true), label="label")
    plt.plot(abs(y_pred), label="pred")
    plt.xlabel("Request index (no unit)")
    plt.ylabel("Absolute Inter Request Time [s]")
    plt.title(description.replace("_", " "))
    plt.legend()
    plt.savefig(Path(model_path, description+".png"))
    if show_plot:
        plt.show()
    plt.close()

    np.save(Path(model_path, 'y_true.npy'), y_true)
    np.save(Path(model_path, 'y_pred.npy'), y_pred)

    # plot different parts of the trace and the corresponding MSE
    l = len(y_true)
    plt_range = min(70, l // 3 - 1)

    # define appropriate locations for plotting: highest irt, lowest irt, random
    idx_high_irt = [np.argmax(y_true[plt_range // 2:l // 3]) + plt_range // 2,
                    # since only idx after plt_range/2 are considered, resulting idx has to be corrected
                    np.argmax(y_true[l // 3:2 * l // 3]) + l // 3,
                    np.argmax(y_true[2 * l // 3:l - plt_range // 2]) + 2 * l // 3]
    idx_low_irt = [np.argmin(y_true[plt_range // 2:l // 3]) + plt_range // 2,
                   np.argmin(y_true[l // 3:2 * l // 3]) + l // 3,
                   np.argmin(y_true[2 * l // 3:l - plt_range // 2]) + 2 * l // 3]
    idx_random_irt = list(np.random.randint(plt_range // 2, l - plt_range // 2 - 1, size=3))
    idx = [idx_high_irt, idx_low_irt, idx_random_irt]

    fig = plt.figure(constrained_layout=True)
    fig.suptitle(description.replace("_", " ")+": Pred=Orange, True=Blue")
    fig.set_size_inches(24, 14)
    row_title = ["High inter request time", "Low inter request", "Random"]
    # create 3x1 subfigs
    subfigs = fig.subfigures(nrows=3, ncols=1)
    for row, (subfig, idx_row) in enumerate(zip(subfigs, idx)):
        subfig.suptitle(f'{row_title[row]}')
        # create 1x3 subplots per subfig
        axs = subfig.subplots(nrows=1, ncols=3)
        for col, (ax, idx) in enumerate(zip(axs, idx_row)):
            begin = idx - plt_range // 2
            end = idx + plt_range // 2
            x_ = range(begin, end)
            y_t = y_true[begin:end]
            y_p = y_pred[begin:end]
            ax.plot(x_, y_t, label="true", color='blue')
            ax.plot(x_, y_p, label="pred", color="orange")
            ax.set_xlabel("Request index (no unit)")
            ax.set_ylabel("Inter Request Time [s]")
            ax.set_title(f'MSE {np.sqrt(mse(y_t, y_p))}')
            np.save(Path(model_path, str(row * 3 + col) + '_idx.npy'), np.array([begin, end]))
    fig.savefig(Path(model_path, description + "eval.png"))
    if show_plot:
        plt.show()
    plt.close()

def predict_online(modelLock, model, X, scaler, log_irt):
    with modelLock:
        start = time.perf_counter()
        prediction = model.predict_on_batch(X.reshape(1, X.shape[0], X.shape[1]))  # predict on the "new" input
        #print(time.perf_counter() - start)
        inf_time.append(time.perf_counter() - start)

    prediction[prediction <= -1] = -1
    prediction[prediction >= 1] = 1
    y_pred = inverse_scale(scaler, prediction)
    if log_irt:  # if log transform is applied to the irt
        y_pred = np.exp(y_pred)
    return y_pred

def predict_online_lite(modelLock, model, X, scaler, log_irt):
    with modelLock:
        input_details = model.get_input_details()
        output_details = model.get_output_details()

        model.set_tensor(input_details[0]["index"], X)
        start = time.perf_counter()
        model.invoke()
        delay = time.perf_counter() - start
        #print(f"inf del: {delay}")
        inf_time.append(delay)
        prediction = model.get_tensor(output_details[0]["index"])

    prediction[prediction <= -1] = -1
    prediction[prediction >= 1] = 1
    y_pred = inverse_scale(scaler, prediction)
    if log_irt:  # if log transform is applied to the irt
        y_pred = np.exp(y_pred)

    return y_pred

def train_online(model, testX, testY, scaler, online_train_interval, log_irt):
    #model = set_lr(online_lr, model)
    if contains_cell(model, LSTM):
        model.reset_states()
    # Online training (update model with each new data available):
    y_pred = []
    loss = []
    train_timer = 0  # training interval, set initial value to 0 to train with the first batch
    Y = []
    X = []
    batch_size=1
    #train process
    for t in tqdm(range(testY.shape[0] // batch_size)):
        x = testX[t * batch_size:(t + 1) * batch_size]
        prediction = model.predict_on_batch(x)  # predict on the "new" input
        prediction[prediction <=-1] = -1
        prediction[prediction >=1] = 1
        prediction = inverse_scale(scaler, prediction)
        X.append(x[0])
        if log_irt:  # if log transform is applied to the irt
            y_pred.extend(list(np.exp(prediction)))
        else:
            y_pred.extend(list(prediction))

        y = testY[t * batch_size:(t + 1) * batch_size].reshape(-1, 1)  # a "new" label is available
        Y.append(y[0])
        if train_timer == 0:
            loss.append(model.fit(np.array(X), np.array(Y), batch_size=batch_size, epochs=1, verbose=0))  # runs a single gradient update
            train_timer = online_train_interval
            Y = []
            X = []
        train_timer = train_timer - batch_size

    return loss, model, y_pred

def evaluate_online(model, test, X, Y, scaler, n_past, online_train_interval, log_irt, start= 0, end = None):
    if end == None:
        end = len(X)
    X = X[start:end]
    Y = Y[start:end]
    loss, model_final, y_pred = train_online(model, X, Y, scaler, online_train_interval, log_irt)
    if log_irt:
        y_true = np.exp(test.iloc[n_past-1:-1]['future inter request time (s)'].to_numpy())
    else:
        y_true = test.iloc[n_past-1:-1]['future inter request time (s)'].to_numpy()

    y_true  = y_true[start:end]
    assert len(y_pred) == len(Y)
    assert len(y_true) == len(y_pred)
    rmse = np.sqrt(mse(y_true, y_pred))
    return y_true, np.array(y_pred), model_final, [rmse, loss]

def train_step(model, loss_fn, optimizer, train_acc_metric, X, Y):
    with tf.GradientTape() as tape:
        logits = model(X, training=True)
        loss_value = loss_fn(Y, logits)
    grads = tape.gradient(loss_value, model.trainable_weights)
    optimizer.apply_gradients(zip(grads, model.trainable_weights))
    if train_acc_metric is not None:
        train_acc_metric.update_state(Y, logits)
    return loss_value

def train_lite(model, X, Y):
    train_ds = tf.data.Dataset.from_tensor_slices((X, Y))
    train_ds = train_ds.batch(1)
    train = model.get_signature_runner("train")
    for x, y in train_ds:
        result = train(x=x, y=y)
    print(f'loss: {result["loss"]}')


def train(model, X, Y):
    if len(Y) < 32:
        return
    optimizer = model.optimizer
    loss_fn = model.loss
    if len(model.metrics) > 0:
        train_acc_metric = model.metrics[1]
    else:
        train_acc_metric = None
    for step, (x_batch_train, y_batch_train) in enumerate(zip(X,Y)):
        x_batch_train=x_batch_train.reshape(1, x_batch_train.shape[0], x_batch_train.shape[1])
        y_batch_train=y_batch_train.reshape(1, y_batch_train.shape[0])
        loss_value = train_step(model, loss_fn, optimizer, train_acc_metric,
                                x_batch_train, y_batch_train)
        print("Training loss (for one batch) at step %d: %.4f" % (step, float(loss_value)))

def load_model(s, study_params, model_name):
    # open trained model
    custom_objects = {}
    if s["hyperparam"]["tcn"]["enable"] == 1:
        custom_objects["TCN"] = TCN
    if study_params["model_loss"] == "custom":
        custom_objects["loss_fn"] = custom_loss(study_params["beta"])
    model_path = Path("model_data",model_name)
    try:
        model = keras.models.load_model(model_path, custom_objects=custom_objects, compile=False)
    except:
        model = keras.models.load_model(Path(model_path, model_name+"_model.h5"), custom_objects=custom_objects, compile=False)
    model.compile(optimizer=keras.optimizers.Adam(learning_rate=study_params["online_lr"]), loss=custom_loss(12),
                  metrics=[RootMeanSquaredError()])
    if contains_cell(model, LSTM):
        model.reset_states()

    return model

def load_lite_model(X, Y, nofthreads, model_name):
    converter = tf.lite.TFLiteConverter.from_saved_model("model_data/"+model_name+"/saved_model")
    converter.target_spec.supported_ops = [
        tf.lite.OpsSet.TFLITE_BUILTINS,  # enable TensorFlow Lite ops.
        tf.lite.OpsSet.SELECT_TF_OPS  # enable TensorFlow ops.
    ]
    converter.optimizations = [tf.lite.Optimize.DEFAULT]
    tflite_model = converter.convert()
    interpreter = tf.lite.Interpreter(model_content=tflite_model, num_threads=nofthreads)
    interpreter.allocate_tensors()
    return interpreter, tflite_model

def evaluate(new_args):
    s, args, study_params, trial_nr = load_environment(new_args)
    df_data, X, Y, scaler = load_workload(args, study_params["n_past"],s["df_part"])
    model, eval_folder_path = load_model(s, study_params)
    # do online training and evaluate result model, test, X, Y, scaler, n_past, online_train_interval, log_irt,
    test_true, test_pred, model_evaluated, loss_online = evaluate_online(model, df_data, X, Y, scaler, study_params["n_past"],
                  study_params["online_train_interval"], args.log_irt)
    model_evaluated.save(Path(eval_folder_path))
    plot_predictions_detailed(test_true, test_pred, eval_folder_path, description = "Online_evaluation_unscaled_test", show_plot=False)
